#
#	this file contains the fundamental classes that keep track of the data
#

import HC_sys

class rec_size:
	"""
	This class holds the size of a record and deals with formatting it/printing it
	size is kept internally as bytes, but printed in human
	readable format as kilobytes, megabytes, and gigabytes
	"""
	def __init__ (self, x = 0):
		"""
		initialize self.siz to zero unless the size was specified as an argument
		"""
		self.siz = x # size is 0 by default
		return

	def print_self (self):
		"""
		print human readable string to a terminal
		"""
		print (self.stringify ())
		return

	def stringify (self):
		"""
		return the size in a human readable string
		"""
		rtrn = ""
		psiz = 0
		sign = ""
		if (self.siz > 1073741824): # then output as gigabytes
			psiz = (self.siz / 1073741824)
			sign = " gb"
		elif (self.siz > 1048576): # then output as megabytes
			psiz = (self.siz / 1048576)
			sign = " mb"
		elif (self.siz > 1024): # then output as kilobytes
			psiz = (self.siz / 1024)
			sign = " kb"
		else: # otherwise barf and just let the huge number through
			psiz = self.siz
			sign = " bytes"
		rtrn = (str (psiz) + sign) # create the human readable string
		return rtrn # return the legible string
	
	def read_size (self):
		"""
		read in a human string (i.e. "2.0 gb")
		and translate it into bytes (actual size)
		"""
		self.t_size = 0
		self.temp = raw_input ("size: ")
		# capture and slice off the sign/denomination (i.e. "gb", "mb", "kb")
		self.load_string (self.temp)
		return
	
	def load_string (self, strng):
		""" capture and slice off the sign/denomination (i.e. "gb", "mb", "kb") """
		if (strng.find ("gb") != -1):
			strng = strng.rstrip (' gb')
			self.t_size = float (strng)
			self.siz = (((self.t_size * 1024) * 1024) * 1024)
		elif (strng.find ("mb") != -1):
			strng = strng.rstrip (' mb')
			self.t_size = float (strng)
			self.siz = ((self.t_size * 1024) * 1024)
		elif (strng.find ("kb") != -1):
			strng = strng.rstrip (' kb')
			self.t_size = float (strng)
			self.siz = (self.t_size * 1024)
		else:
			if (strng.isdigit ()):
				self.t_size = float (strng)
				self.siz = self.t_size
			else:
				#print ("rec_size::load_string(): invalid size")
				self.siz = 0
		return

class record:
	"""
	This class holds the data pertaining to a single record:
		- name, size, censored, subs, dubs, format, quality, comment
	"""
	def __init__ (self, nam = "", S = 0, cen = "", sub = "", dub = "", form = "", qual = "", comnt = ""):
		self.nam = nam
		self.ssize = rec_size (S)
		self.censored = cen
		self.subs = sub
		self.dubs = dub
		self.format = form
		self.quality = qual
		self.comment = comnt
		return

	def print_self (self, switch = 0):
		if (switch == 0):
			print ("    name: " + self.nam)
			print ("    size: " + self.ssize.stringify ())
			print ("censored: " + self.censored)
			print ("    subs: " + self.subs)
			print ("    dubs: " + self.dubs)
			print ("  format: " + self.format)
			print (" quality: " + self.quality)
			print ("comments: " + self.comment)
		elif (switch == 1):
			print ("[0]    name: " + self.nam)
			print ("[1]    size: " + self.ssize.stringify ())
			print ("[2]censored: " + self.censored)
			print ("[3]    subs: " + self.subs)
			print ("[4]    dubs: " + self.dubs)
			print ("[5]  format: " + self.format)
			print ("[6] quality: " + self.quality)
			print ("[7]comments: " + self.comment)
		return
	
	def read_name (self):
		self.nam = raw_input ("name: ")
		return
		
	def read_censored (self):
		self.censored = raw_input ("censored: ")
		return
	
	def read_subs (self):
		self.subs = raw_input ("subs: ")
		return
		
	def read_dubs (self):
		self.dubs = raw_input ("dubs: ")
		return
	
	def read_size (self):
		self.ssize.read_size ()
		return
	
	def read_format (self):
		self.format = raw_input ("format: ")
		return
		
	def read_quality (self):
		self.quality = raw_input ("quality: ")
		return
	
	def read_comment (self):
		self.comment = raw_input ("comments: ")
		return
	
	def load_data_list (self, data_list):
		""" grab all the data from the incomming arg- I hope that data is all in the right order """
		self.nam = data_list [0]
		self.ssize.load_string (data_list [1])
		self.censored = data_list [2]
		self.subs = data_list [3]
		self.dubs = data_list [4]
		self.format = data_list [5]
		self.quality = data_list [6]
		self.comment = data_list [7]
		return
	
	def load_from_file (self, fil):
		""" strip everything out of the file one line at a time then store it """
		self.temp_dat_list = [0,0,0,0,0,0,0,0]
		# read 8 lines from the file
		self.i = 0
		while (self.i < 8):
			self.temp_dat_list [self.i] = fil.readline ().strip ()
			self.i = self.i + 1
		self.load_data_list (self.temp_dat_list)
		return
	
	def set_field (self, ind):
		if (ind == 0): # nam
			self.read_name ()
		elif (ind == 1): # size
			self.read_size ()
		elif (ind == 2): # censored
			self.read_censored ()
		elif (ind == 3): # subs
			self.read_subs ()
		elif (ind == 4): # dubs
			self.read_dubs ()
		elif (ind == 5): # format
			self.read_format ()
		elif (ind == 6): # quality
			self.read_quality ()
		elif (ind == 7): # comments
			self.read_comments ()
		else: # if we get here then barf and quit
			print ("record::set_field(): invalid field index")
		return
	
	def cmp_names (self, other):
		""" return 1 if self > other, -1 if self < other, 0 if self == other """
		if (self.nam > other.nam):
			self.rtrn = 1
		elif (self.nam == other.nam):
			self.rtrn = 0
		else: # (self.nam < other.nam):
			self.rtrn = -1
		return self.rtrn
	
	def cmp_sizes (self, other):
		""" return 1 if self > other, -1 if self < other, 0 if self == other """
		if (self.ssize.siz > other.ssize.siz):
			self.rtrn = 1
		elif (self.ssize.siz == other.ssize.siz):
			self.rtrn = 0
		else: # (self.ssize.siz < other.ssize.siz):
			self.rtrn = -1
		return self.rtrn

class rec_list:
	"""
	This class holds a list of records and deals with:
		printing them nicely on screen
		writing/saving them nicely to file
		reading/loading them from file
		sorting them by name, size etc.
		adding, deleting, and editing them
	"""

	def __init__ (self):
		# this creates the self.lizt member variable ( a list ) and makes it empty by default
		self.lizt = []
		return

	def add (self, R):
		""" add the arg record to the list """
		self.lizt.append (R)
		return

	def del_ind (self, i):
		""" delete the record at the arg index """
		self.lizt.pop (i)
		return

	def del_nam (self, S):
		""" delete the record which has the arg name """
		for x in self.lizt:
			if (x.nam == S):
				self.lizt.remove (x)
				return
		print ("no record with name found [" + S + "]")
		return
	
	def edit_ind (self, i):
		""" edit the record at index i """
		self.c = " "
		while (self.c != "x"):
			# print_self(1) will print with numbered tags next to each field
			self.lizt [i].print_self (1)
			self.c = raw_input ("edit which field? ('x' to exit) ")
			if ((self.c != "x") and (str.isdigit (self.c))):
				# edit the field
				self.lizt [i].set_field (int (self.c))
		return
	
	def append_list (self, other_rec_list):
		""" append the contents of other_rec_list to self.lizt """
		self.lizt.extend (other_rec_list.lizt)
		return
	
	def search_name (self, key):
		"""Find the entry with name matching key and return a tuple with success/failure and it's index."""
		self.index = 0
		self.rtrn = []
		for x in self.lizt:
			if (key in self.lizt [self.index].nam):
				self.rtrn.append ((int (self.index), self.lizt [self.index].nam))
				self.index = self.index + 1
			else:
				self.index = self.index + 1
		return self.rtrn
	
	def index_valid (self, check_ind):
		self.rtrn = False
		if ((len (self.lizt) != 0) and (check_ind >= 0) and (check_ind < len (self.lizt))):
			self.rtrn = True
		return self.rtrn
	
	def print_ind (self, i):
		"""Print contents of a specific index."""
		self.lizt [i].print_self ()
		return
		
	def print_list (self, S):
		if ((S == "matching") or (S == "m") or (S == 0)):
			k = raw_input ("matching what?: ")
			print ("printing matching " + k + "...")
			print (HC_sys.sep_str_eq)
			for x in self.lizt:
				if (k in x.nam):
					x.print_self ()
					print (HC_sys.sep_str_dl)
			print (HC_sys.sep_str_eq)
		elif ((S == "names") or (S == "n") or (S == 1)): # including singular and plural for 'name/s' just for convenience
			print ("printing -names_only...")
			print (HC_sys.sep_str_eq)
			for x in self.lizt:
				print (x.nam)
			print (HC_sys.sep_str_eq)
		elif ((S == "tagged") or (S == "t")):
			print ("printing -names_only...")
			print (HC_sys.sep_str_eq)
			ind = 0
			for x in self.lizt:
				print ('[' + str (ind) + '] ' + x.nam)
				ind = ind + 1
			print (HC_sys.sep_str_eq)
		elif (S.isdigit () == True):
			print ("printing index [" + S + "]")
			print (HC_sys.sep_str_eq)
			self.lizt [int (S)].print_self ()
			print (HC_sys.sep_str_eq)
		elif ((S == "all") or (S != "all") or (S == 2)): # if the option isn't -all or any of the above then print -all
			if (S != "all"):
				print ("invalid option. printing -all by default:")
			else:
				print ("printing all...")
			print (HC_sys.sep_str_eq)
			for x in self.lizt:
				x.print_self ()
				print (HC_sys.sep_str_dl)
			print (HC_sys.sep_str_eq)
		return
	
	def cmp_by_names (self, rec_one, rec_two):
		return rec_one.cmp_names (rec_two)
	
	def cmp_by_sizes (self, rec_one, rec_two):
		return rec_one.cmp_sizes (rec_two)
	
	def sort_list (self, sort_by = 0):
		""" sort the list of records by name, by size, or some other field """
		if ((sort_by == "name") or (sort_by == 0)):
			self.lizt.sort (self.cmp_by_names)
		elif ((sort_by == "size") or (sort_by == 1)):
			self.lizt.sort (self.cmp_by_sizes)
		return
	
#	def load_rec (self, fil):
#		self.rtrn_rec = record ()
#		self.data_list = []
#		self.x = 0
#		self.continu = True
#		# rip the data out of the file, one line at a time
#		while (self.continu == True and self.x < 8):
#			self.inner = fil.readline ()
#			if (self.inner != ']' and self.inner != '^^' and self.inner != '' and self.inner != '\n'):
#				self.data_list.push (self.inner [0:-1]) #  = the incomming line minus the '\n' at the end
#				self.x = self.x + 1
#			else:
#				self.continu = False
#		
#		# stuff the data into rtrn_rec
#		self.rtrn_rec.load_data_list (self.data_list)
#		return self.rtrn_rec
	
	def save_file (self, filename):
		""" save everything out to a file. preference to sort before saving is False by default for efficiency """
		self.x = 0
		self.fil = open (filename, 'w')
			
		#now save all the records
		while (self.x < len (self.lizt)):
			self.fil.write ('[\n')
			self.fil.write (self.lizt [self.x].nam + '\n')
			self.fil.write (self.lizt [self.x].ssize.stringify () + '\n')
			self.fil.write (self.lizt [self.x].censored + '\n')
			self.fil.write (self.lizt [self.x].subs + '\n')
			self.fil.write (self.lizt [self.x].dubs + '\n')
			self.fil.write (self.lizt [self.x].format + '\n')
			self.fil.write (self.lizt [self.x].quality + '\n')
			self.fil.write (self.lizt [self.x].comment + '\n')
			self.fil.write (']\n')
			self.x = self.x + 1
		self.fil.write ('^^\n')	# write out our EOF symbol at the end even though Python has its own ways of recognizing
		self.fil.write ('\n')	# the end of a file I'm a belt-and-suspenders man. Besides, this provides other/later programs
		self.fil.close ()		# with a landmark to go by- universality is its own reward
		return
	
	def load_file (self, filename):#, comment_buff):
		""" load everything from a file """
		self.i = '000'
		self.y = 0
		self.continu = True
		
		if (filename == ""):
			return
		else:
			self.fil = open (filename, "r")
		
		
		#for self.i in range (len (comment_buff)):				# clear out the comment_buffer-- we'll be stuffing things into it
		#	comment_buff.pop ()
			
		for self.i in range (len (self.lizt)):					# clear out the currently loaded record list
			self.lizt.pop ()
			
		while (self.i != '' and self.i != '^^'):				# run this loop 'till we hit the EOF
			self.i = self.fil.readline ().strip ()
			if (self.i == '['):									# detected the beginning of a record
				self.new_rec = record ()
				self.new_rec.load_from_file (self.fil)			# we can just pass self.fil to the record because self.fil remembers the current file position
				self.lizt.append (self.new_rec)
			elif ('#' in self.i):								# that's a comment line, so put it into the comment buffer and keep going
				#comment_buff.append (self.i [1:-1])			# slice off the '#' at the beginning and '\n' at the end
				pass
			elif ('^^' in self.i or self.i == ''):				# making damn sure we don't run over the EOF
				#this is the end of the file so stop
				pass
		self.fil.close ()
		return
	
	def concat_file (self, filename):
		""" load the given filename and add its contents onto the end of our list """
		self.other_list = rec_list ()
		self.other_list.load_file (filename)
		self.append_list (self.other_list)
		return
	
	def append_to_file (self, filename):
		""" append our list onto the end of the contents of the given file """
		self.other_list = rec_list ()
		self.other_list.load_file (filename)
		self.append_list (self.other_list)
		self.save_file (filename)
		return

