#
#	This file contains the preferences:: class which is responsable for loading, saving, and handling preferences
#

import os
import sys
import types
from config import Config

class preferences:
	def __init__ (self, filename = "/hc.cfg"):
		"""
		set the preferences to default values and create the .cfg file if it doesn't already exist
		"""
		
		# a list of the last five save files that were opened-- we keep this here rather than in self.conf because we can manipulate it easier here (vis-a-vis push() and pop())
		self.last_five = ["", "", "", "", ""]
			
		self.cfg_filename = sys.path [0] + filename
		
		if (os.path.exists (self.cfg_filename) == False):
			self.make_clean_conf ()
		else:
			self.load (self.cfg_filename)
		return
	
	def print_self (self):
		print ("[0] Sort on add: " + str (self.conf.sort_on_add))
		print ("[1] Sort on save: " + str (self.conf.sort_on_save))
		print ("[2] Auto mount directory: " + str (self.conf.auto_mount_dir))
		print ("[3] Auto load last: " + str (self.conf.auto_load))
		print ("[4] Display Splash: " + str (self.conf.print_splash))
		return
	
	def str_to_bool (self, string):
		self.t_values = ["true", "True", "t", "y", "1"]
		if string in self.t_values:
			self.rtrn = True
		else:
			self.rtrn = False
		return self.rtrn
	
	def set_field (self, field_index, value):
		""" return True if the field was set. return False if it wasn't """
		self.rtrn = False
		if (field_index == 0):
			self.rtrn = self.set_sort_on_add (self.str_to_bool (value))
		elif (field_index == 1):
			self.rtrn = self.set_sort_on_save (self.str_to_bool (value))
		elif (field_index == 2):
			self.rtrn = self.set_auto_mount_dir (value)
		elif (field_index == 3):
			self.rtrn = self.set_auto_load (self.str_to_bool (value))
		elif (field_index == 4):
			self.rtrn = self.set_print_splash (self.str_to_bool (value))
		return self.rtrn
	
	def make_clean_conf (self):
		""" make a clean .cfg file in the default location """
		self.conf = Config ()
		self.conf.sort_on_add = False
		self.conf.sort_on_save = False
		self.conf.auto_mount_dir = ""
		self.conf.auto_load = False
		self.conf.print_splash = True
		self.conf.last_five = ['','','','','']
		self.cfg_file = open (self.cfg_filename, 'w')
		self.conf.save (self.cfg_file)
		return
	
	def load (self, filename = ""):
		""" load up all the preferences in the given filename """
		# if no argument was given then we'll use the default that was set up in __init__()
		if (filename == ""):
			self.save_filename = self.cfg_filename
		else:
			self.save_filename = filename
		
		if (os.path.exists (self.save_filename) == True):
			self.cfg_file = open (self.save_filename, 'r')
			self.conf = Config (self.save_filename)
			x = 0
			for each in self.conf.last_five:
				self.last_five [x] = self.conf.last_five [x]
				x = x + 1
		else:
			print ("file not found: " + self.save_filename)
		return
	
	def save (self, filename = ""):
		""" save all the preferences to the given filename """
		# check to be sure the .cfg file exists, if not then create it and put our default settings into it
		if (filename == ""):
			self.save_filename = self.cfg_filename
		else:
			self.save_filename = filename
		
		self.cfg_file = open (self.save_filename, 'w')
		self.conf.save (self.cfg_file)
		return
	
	def set_sort_on_add (self, value):
		if type (value) is types.BooleanType:
			self.conf.sort_on_add = value
			self.rtrn = True	# true if we did change the setting
		else:
			self.rtrn = False	# false if we did nothing (i.e. a bad value type)
		return self.rtrn
	
	def set_sort_on_save (self, value):
		if type (value) is types.BooleanType:
			self.conf.sort_on_save = value
			self.rtrn = True
		else:
			self.rtrn = False
		return self.rtrn
	
	def set_auto_mount_dir (self, value):
		if type (value) is types.StringType:
			self.conf.auto_mount_dir = value
			self.rtrn = True
		else:
			self.rtrn = False
		return self.rtrn
	
	def set_auto_load (self, value):
		if type (value) is types.BooleanType:
			self.conf.auto_load = value
			self.rtrn = True
		else:
			self.rtrn = False
		return self.rtrn
	
	def set_print_splash (self, value):
		if type (value) is types.BooleanType:
			self.conf.print_splash = value
			self.rtrn = True
		else:
			self.rtrn = False
		return self.rtrn
	
	def add_to_last_five (self, value):
		""" we'll automatically pop off the oldest of the last five """
		if type (value) is types.StringType:
			self.last_five.pop ()				# pop off the end
			self.last_five.insert (0, value)	# insert at the front
			self.conf.last_five = self.last_five
			self.rtrn = True
		else:
			self.rtrn = False
		return self.rtrn
	
	def get_sort_on_add (self):
		return self.conf.sort_on_add
	
	def get_sort_on_save (self):
		return self.conf.sort_on_save
	
	def get_auto_mount_dir (self):
		return self.conf.auto_mount_dir
	
	def get_auto_load (self):
		return self.conf.auto_load
	
	def get_print_splash (self):
		return self.conf.print_splash
	
	def get_last_five (self):
		return self.last_five

