#
#	this file contains the command line interface
#
import os
import sys
import HC_sys
import catalog_lib
import prefs_lib


def help ():
	"""
	print out the available arguments
	"""
	print ("Cataloger help:")
	print ("-h\t\tPrint this help message.")
	print ("-v\t\tPrint version info.")
	print ("-s\t\tSort the given .lzt file alphabetically.")
	print ("\t\t\t-s=/home/me/my_list_dir/my_list_file.lzt")
	print ("-p\t\tPrint the given .lzt file to standard out.")
	print ("\t\t\t-p=/home/me/my_list_dir/my_list_file.lzt")
	print ("-dir\t\tSet the working directory.")
	print ("\t\t\t-dir=/mnt/my_drive/some_dir/dir_I_want_to_work_on")
	print ("-batren\t\tBatch rename files in the working directory.")
	print ("\t\t\t-batren=SOME_TEXT_###")
	print ("\t\t\t'#' will be replaced with a padded integer: ### ---> 000, 001, 002 ...")
	print ("-tag\t\tTag all files in the working directory with the given tag(s).")
	print ("\t\t\t-tag=[MY_FIRST_TAG][MY_SECOND_TAG][MY_THIRD_TAG]")
	print ("-scan\t\tScan all the files in the working directory into a .lzt file in the given directory.")
	print ("\t\t\t-scan=/home/me/my_list_dir/my_list_file.lzt")
	print ("\t\t\tThe resulting .lzt file is sorted before being written to disk. This can be slow")
	print ("\t\t\tIf no fully qualified path is given then the file is saved in the current working directory")
	print ("-fastscan\tSame as -scan, but the .lzt file is not sorted.")
	print ("-killdups\tFind all duplicates in the working directory and delete them.")
	return

def printVersion ():
	"""
	print out the version string
	"""
	print (HC_sys.HC_version_string)
	return

def sort (listFileName):
	"""
	sort the given .lzt file alphabetically
	"""
	# see if the file exists (i.e. is an absolute path)
	# if so then sort it
	if (os.path.exists (listFileName)):
		recs = catalog_lib.rec_list ()
		recs.load_file (listFileName)
		recs.sort_list (0) # sort by names
		recs.save_file (listFileName) # now save it
		return True
	# see if maybe the file is in the current working directory or is a relative path
	elif (os.path.exists (HC_sys.cwd () + listFileName)):
		recs = catalog_lib.rec_list ()
		recs.load_file (HC_sys.cwd () + listFileName)
		recs.sort_list (0) # sort by names
		recs.save_file (HC_sys.cwd () + listFileName) # now save it
		return True
	# if we can't find it then barf and quit
	else:
		print ("File not found: " + listFileName)
		return False

def printListFile (listFileName):
	"""
	print out the contents of the .lzt file
	"""
	# see if the file exists, if so then print it
	if (os.path.exists (listFileName)):
		recs = catalog_lib.rec_list ()
		recs.load_file (listFileName)
	# see if maybe the file is in the current working directory
	elif (os.path.exists (HC_sys.cwd () + listFileName)):
		recs = catalog_lib.rec_list ()
		recs.load_file (HC_sys.cwd () + listFileName)
	# if we can't find it then barf and quit
	else:
		print ("File not found: " + listFileName)
		return False
		
	recs.print_list ("all")
	return True

def setWorkingDir (newWorkingDir):
	"""
	set the workingDir to the given dir (but only if it's valid)
	"""
	rtrn = HC_sys.change_dir (newWorkingDir)
	if (rtrn == False):
		print ("Not a directory (or I can't find it because I'm dumb): " + newWorkingDir)
	return rtrn

def batchRename (pattern):
	"""
	#'s are turned into a padded integer: ## = 00, 01, 02...; ### = 000, 001, 002...
	text is put before the padded integer
	"""
	padding = pattern.count ("#") # how many #'s do we have? that's how many digits we have
	pattern = pattern.strip ("#") # now take 'em out
	HC_sys.batch_rename (pattern, padding)
	return

def tag (tagString):
	"""
	all tags are stripped of spaces and stacked up at the end of the file names but before any trailing numbers
	"""
	HC_sys.batch_tag (tagString)
	return

def scanDir (listFileName):
	"""
	scan all files in the workingDir into a .lzt file
	the .lzt file is sorted before being saved
	(might be kinda slow if it's a big list)
	"""
	newList = HC_sys.scan_cwd ()
	newList.sort_list (0) # sort by name
	newList.save_file (listFileName)
	return
	
def fastScanDir (listFileName):
	"""
	scan all files in the workingDir into a .lzt file
	the .lzt file is not sorted before being saved
	(this is a little faster)
	"""
	newList = HC_sys.scan_cwd ()
	newList.save_file (listFileName)
	return

def killDups ():
	"""
	destroy all duplicate files in the workingDir
	"""
	HC_sys.kill_dup_files ()
	return

#
#	set me up before executing me
#
class hcCLI_command:
	"""
	a handle to a method and its argument
	when it's time to run the command just call self.execute()
	"""
	def __init__ (self, methodHandle = None, argString = None):
		self.methodHandle = methodHandle
		self.argString = argString
		return
	
	def execute (self):
		if (self.methodHandle != None):
			if (self.argString != None):
				return self.methodHandle (self.argString)
			else:
				return self.methodHandle ()
		return


#
#	figure out if the commands are ok, then execute them
#
class hcCLI_controller:
	"""
	figure out if the arguments are ok,
	keep track of which ones need to be executed,
	and keep track of the arguments they need
	"""
	def __init__ (self):
		# list of args to execute, and their arguments
		self.commandsList = []
		return
		
	def validateArgs (self, argList):
		"""
		return True if all arguments are valid
		return False otherwise
		----------------------------------------
		also build a list of all commands for execution
		"""
		self.argOK = True
		x = 0
		while ((self.argOK) and (x < (len (argList) - 1))):
			x = x + 1
			if (argList [x] [0:2] == "-h"):
				self.argOK = True
				self.commandsList.append (hcCLI_command (help))
			elif (argList [x] [0:2] == "-v"):
				self.argOK = True
				self.commandsList.append (hcCLI_command (printVersion))
			elif (argList [x] [0:3] == "-s="):
				self.argOK = True
				self.commandsList.append (hcCLI_command (sort, argList [x] [3:]))
			elif (argList [x] [0:3] == "-p="):
				self.argOK = True
				self.commandsList.append (hcCLI_command (printListFile, argList [x] [3:]))
			elif (argList [x] [0:5] == "-dir="):
				self.argOK = True
				# we set the working directory immediately: before any other command executes
				if (setWorkingDir (argList [x] [5:]) != True):
					print ("Directory doesn't seem to exist: ", argList [x] [5:])
					return False
			elif (argList [x] [0:8] == "-batren="):
				self.argOK = True
				self.commandsList.append (hcCLI_command (batchRename, argList [x] [8:]))
			elif (argList [x] [0:5] == "-tag="):
				self.argOK = True
				self.commandsList.append (hcCLI_command (tag, argList [x] [5:]))
			elif (argList [x] [0:6] == "-scan="):
				self.argOK = True
				self.commandsList.append (hcCLI_command (scanDir, argList [x] [6:]))
			elif (argList [x] [0:10] == "-fastscan="):
				self.argOK = True
				self.commandsList.append (hcCLI_command (fastScanDir, argList [x] [10:]))
			elif (argList [x] [0:10] == "-killdups"):
				self.argOK = True
				self.commandsList.append (hcCLI_command (killDups))
			else:
				self.argOK = False
		return self.argOK
	
	
	def executeCommands (self):
		"""
		execute all the commands.
		(they should already have been set up in self.validateArgs())
		"""
		for eachCommand in self.commandsList:
			eachCommand.execute ()
		return


def cli_main ():
	"""
	validate the command line arguments
	then execute them
	"""
	C = hcCLI_controller ()
	
	# validate all command line args
	if (C.validateArgs (sys.argv) == False):
		print ("Invalid arguments.")
		help ()
		print ("\nFailure. Nothing was done.")
		return False
	else:
		C.executeCommands ()
		return True

