#
#	classes used to implement widgets in the GUI itself.
#	the functionality is mainly in catalog_lib.py and HC_sys.py
#

from Tkinter import *
import os
import catalog_lib
import HC_sys

#
#	a box to display details about a record (i.e. one selected in the record listbox)
#
class rec_detail_box (Frame):
	def __init__ (self, parent, **kwargs):
		Frame.__init__ (self, parent, **kwargs)
		self.parent = parent
		
		# a list of prefix strings used to label data
		self.strings = ["Name: ", "Size: ", "Cens: ", "Subs: ", "Dubs: ", "Form: ", "Qual: ", "Comm: "]
		# a list of labels that will hold and display the data
		self.fields = []
		# build and draw all the labels
		self.build_body ()
		return
	
	def build_body (self):
		for self.x in range (0, 7):
			self.fields.append (Label (self, text = self.strings [self.x]))
			self.fields [self.x].grid (row = self.x, column = 0, sticky = W)
		return
	
	def set_data (self, rec):
		self.fields [0].configure (text = self.strings [0] + rec.nam)
		self.fields [1].configure (text = self.strings [1] + rec.ssize.stringify ())
		self.fields [2].configure (text = self.strings [2] + rec.censored)
		self.fields [3].configure (text = self.strings [3] + rec.subs)
		self.fields [4].configure (text = self.strings [4] + rec.dubs)
		self.fields [5].configure (text = self.strings [5] + rec.format)
		self.fields [6].configure (text = self.strings [6] + rec.quality)
#		self.fields [7].configure (text = self.strings [7] + rec.comment)
		return
	
	def clear_data (self):
		for self.x in range (0, 7):
			self.fields [self.x].configure (text = self.strings [self.x])
		return

#
#	a listbox with a scroll bar (just makes life easier)
#
class scrolled_listbox (Frame):
	def __init__ (self, parent, **kwargs):
		Frame.__init__ (self, parent, **kwargs)
		self.parent_handle = parent
		self.listbox = Listbox (self, **kwargs)
		self.scrollbar = Scrollbar (self, command = self.listbox.yview, orient = VERTICAL)
		self.listbox.configure (yscrollcommand = self.scrollbar.set)
		self.scrollbar.pack (side = RIGHT, expand = NO, fill = Y)
		self.listbox.pack (side = LEFT, expand = YES, fill = BOTH)
		return

#
#	a simple box to display a string of text
#
class feedback_box (Frame):
	def __init__ (self, parent, style = 0, **kwargs):
		Frame.__init__ (self, parent, **kwargs)
		self.parent_handle = parent
		
		return

#
#	display a list of records with scrollbar, and handle item interaction
#
class rec_list_box (Frame):
	def __init__ (self, parent, detail_box, r_list, **kwargs):
		Frame.__init__ (self, parent, **kwargs)
		self.parent_handle = parent
		self.detail_box_handle = detail_box
		self.r_list_handle = r_list
		
		self.build_body ()
		return
	
	def build_body (self):
		self.rec_listbox = Listbox (self, width = 32, height = 24) # the big enchilada list of all the records
		self.rec_listbox.bind ("<Button-1>", self.onClick)
		self.rec_listbox.bind ("<Delete>", self.onDelete)
		self.rec_listbox.bind ("<<ListboxSelect>>", self.onSelect)
		self.reclistbox_scrlbar = Scrollbar (self, command = self.rec_listbox.yview, orient = VERTICAL)
		self.rec_listbox.configure (yscrollcommand = self.reclistbox_scrlbar.set)
		self.reclistbox_scrlbar.pack (side = RIGHT, expand = NO, fill = Y)
		self.rec_listbox.pack (side = LEFT, expand = YES, fill = BOTH)		# give it as much space as possible
		return
	
	def onClick (self, event = None):
		# set the selection to the item at the current mouse coordinates
		self.mouse_coords = "@" + str (event.x) + "," + str (event.y)
		event.widget.activate (event.widget.index (self.mouse_coords))
		# use the selection
		self.onSelect (event)
		return
	
	def onSelect (self, event = None):
		if (self.r_list_handle.index_valid (event.widget.index (ACTIVE)) == True):
			self.detail_box_handle.set_data (self.r_list_handle.lizt [event.widget.index (ACTIVE)])
		return
	
	def add (self, new_rec):
		self.r_list_handle.add (new_rec)
		self.refresh ()
		return
		
	def delete (self, index):
		if (self.r_list_handle.index_valid (index) == True):
			self.rtrn = self.r_list_handle.lizt [index]
			self.r_list_handle.del_ind (index)
		self.detail_box_handle.clear_data ()
		self.refresh ()
		return self.rtrn
	
	def sort (self, mode):
		if (mode == 0):
			self.r_list_handle.sort_list ("name")
		elif (mode == 1):
			self.r_list_handle.sort_list ("size")
		self.refresh ()
		return
	
	def onDelete (self, event = None):
		if (event == None):	# if event == None then we were called via button
			self.r_list_handle.del_ind (self.rec_listbox.index (ACTIVE))
		else:				# if we got an event then we were called via the <Delete> key
			self.r_list_handle.del_ind (event.widget.index (ACTIVE))
		
		# refresh the listbox and clear the detailbox
		self.detail_box_handle.clear_data ()
		self.refresh ()
		return
	
	def refresh (self):
		# refresh myself so that if any record names changed I can show that
		self.rec_listbox.delete (0, END)
		for each in self.r_list_handle.lizt:
			self.rec_listbox.insert (END, each.nam)
		return

#
#	something to show progress of a task
#
class progress_bar:
    def __init__(self, master = None, labelText = "", dolabel = 1, orientation = "horizontal",
				 minimum = 0, maximum = 100, width = 100, height = 18, appearance = "sunken",
				 fillColor = "darkblue", background = "gray", labelColor = "white",
				 labelFont = "Verdana", labelFormat = "%d%%", value = 50, bd = 2):
		# preserve various values
		self.master = master
		self.labelText = labelText
		self.doLabel = dolabel
		self.orientation = orientation
		self.min = minimum
		self.max = maximum
		self.width = width
		self.height = height
		self.fillColor = fillColor
		self.labelFont = labelFont
		self.labelColor = labelColor
		self.background = background
		self.labelFormat = labelFormat
		self.value = value

		self.frame = Frame (master, relief = appearance, bd = bd)
		self.canvas = Canvas (self.frame, height = height, width = width, bd = 0, highlightthickness = 0, background = background)
		self.scale = self.canvas.create_rectangle (0, 0, width, height, fill = fillColor)
		self.label = self.canvas.create_text (self.canvas.winfo_reqwidth () / 2, height / 2, text = labelText, anchor = "c", fill = labelColor, font = self.labelFont)
		self.update ()
		self.canvas.pack (side = TOP, expand = NO, fill = X)
		return

    def update_progress (self, newValue, newMax = None):
		if newMax:
			self.max = newMax
		self.value = newValue
		self.update ()
		return

    def update (self):
		# Trim the values to be between min and max
		if (self.value > self.max):
			self.value = self.max
		if (self.value < self.min):
			self.value = self.min
		# Adjust the rectangle
		if (self.orientation == "vertical"):
			self.canvas.coords (self.scale, 0, self.height - (float (self.value) / self.max * self.height), self.width, self.height)
		else: # "horizontal" by default
			self.canvas.coords (self.scale, 0, 0, float (self.value) / self.max * self.width, self.height)
		
		# Now update the colors
		self.canvas.itemconfig (self.scale, fill = self.fillColor)
		self.canvas.itemconfig (self.label, fill = self.labelColor)
		
		# And update the label
		if (self.doLabel):
			if (self.value):
				if (self.value >= 0):
					pvalue = int ((float (self.value) / float (self.max)) * 100.0)
				else:
					pvalue = 0
				self.canvas.itemconfig (self.label, text = self.labelFormat % pvalue)
			else:
				self.canvas.itemconfig (self.label, text = '')
		else:
			self.canvas.itemconfig (self.label, text = self.labelFormat % self.labelText)
		
		self.canvas.update_idletasks()
		return

#
#	a dialog to rename all the files in the current working directory
#
class rename_dialog (Toplevel):
	def __init__ (self, parent, title = ""):
		Toplevel.__init__ (self, parent)
		self.transient (parent)
		
		self.title (title)
		self.parent = parent
		# variables to hold values from the widgets
		self.replace_spaces = IntVar ()
		self.was_canceled = True
		
		self.initial_focus = self.build_body ()					# construct all the widgets, lay them out etc
		
		self.build_okcancel ()									# build the ok/cancel buttons on the bottom of the window
		
		self.grab_set ()										# take over keyboard input
		
		if not self.initial_focus:
			self.initial_focus = self
		
		self.protocol ("WM_DELETE_WINDOW", self.cancel)
		self.geometry ("+%d+%d" % (parent.winfo_rootx () + 50, parent.winfo_rooty () + 50))
		self.initial_focus.focus_set ()							# set focus to the widget that should have it
		self.wait_window (self)
		return
	
	def build_body (self):
		# display the current working directory
		self.curr_dir_frame = Frame (self)
		self.curr_dir_frame.pack (side = TOP, expand = YES, fill = BOTH)
		Label (self.curr_dir_frame, text = os.getcwd (), relief = RAISED).pack (side = TOP, expand = YES, fill = BOTH)
		# all the options go in here
		self.root_frame = Frame (self)
		# name field
		Label (self.root_frame, text = "Name").grid (row = 0, column = 0, sticky = W)
		self.name_field = Entry (self.root_frame)
		self.name_field.grid (row = 1, column = 0, columnspan = 2)
		# how many digits do they want to use to pad their numbers
		Label (self.root_frame, text = "Number of padding digits").grid (row = 8, column = 0, sticky = W)
		self.num_pad_field = Entry (self.root_frame)
		self.num_pad_field.grid (row = 9, column = 0)
		# general options
		Label (self.root_frame, text = "Options").grid (row = 10, column = 0, sticky = W)
		self.space_to_score_box = Checkbutton (self.root_frame, text = "Replace spaces\n with underscores", variable = self.replace_spaces)
		self.space_to_score_box.grid (row = 11, column = 0, sticky = W)
		# show everything
		self.root_frame.pack (side = TOP)
		return self.name_field
	
	def build_okcancel (self):
		# add standard button box. override if you don't want the
		# standard buttons
		self.okcancel_box = Frame (self)

		self.ok_button = Button (self.okcancel_box, text = "OK", command = self.ok, default = ACTIVE)
		self.ok_button.pack (side = LEFT)
		self.cancel_button = Button (self.okcancel_box, text = "Cancel", command = self.cancel)
		self.cancel_button.pack (side = LEFT)

		self.bind ("<Return>", self.ok)
		self.bind ("<Escape>", self.cancel)

		self.okcancel_box.pack (side = BOTTOM)
		return
	
	def ok (self, event = None):
		# only execute this function if it was hit from the OK button
		# if we don't do this check then the user could hit <Return> while
		# focused on one of the checkboxes/radiobuttons and that would toggle
		# the button while still exiting the window-- in that case it would appear
		# as though a button wasn't working when, in fact, it had simply been
		# toggled a bare instant before the window exited
		if ((event != None) and (event.widget.widgetName != "button")):
			return
		
		self.withdraw ()
		self.update_idletasks ()
		
		# figure out what the base string needs to be
		self.base_string = self.name_field.get ()
		
		# if the padding isn't empty and is valid then use it
		if ((self.num_pad_field.get () != "") and (self.num_pad_field.get ().isdigit () == True)):
			self.num_pad = int (self.num_pad_field.get ())
		else:
			self.num_pad = 1
		
		# if spaces need to be underscores then make it so
		if (self.replace_spaces.get () == 1):
			self.base_string = self.base_string.replace (" ", "_")
		
		# execute
		self.dir_list = os.listdir (os.getcwd ())
		self.dir_list.sort ()
		self.x = 0
		for each_file in self.dir_list:
			if (os.path.isdir (each_file) == False):			# only rename it with file extension if it's a file
				self.ext_str = "." + each_file.rsplit (".") [1]	# grab the extension from the original filename
				os.rename (each_file, (self.base_string + str (self.x).zfill (self.num_pad)) + self.ext_str)
				self.x = self.x + 1
		
		self.was_canceled = False
		self.parent.focus_set ()
		self.destroy ()
		return
	
	def cancel (self, event = None):
		# put focus back to the parent window
		self.was_canceled = True
		self.parent.focus_set ()
		self.destroy ()
		return

#
#	dialog to manually add a record
#
class add_rec_dialog (Toplevel):
	def __init__ (self, parent, title = "", ed_rec = None):
		Toplevel.__init__ (self, parent)
		self.transient (parent)
		self.title (title)
		self.parent = parent
		
		self.name_field_str = StringVar ()						# name field
		self.cen_uncen = StringVar ()							# "Censored" or "Uncensored"
		self.size_field_str = StringVar ()						# is automatically translated into bytes
		self.subs_field_str = StringVar ()						#
		self.dubs_field_str = StringVar ()						#
		self.form_field_str = StringVar ()						#
		self.qual_value = StringVar ()							# "Excellent", "Good", "Poor"
		self.comm_field_str = StringVar ()						#
		
		self.cen_uncen.set ("Uncensored")						# init the values of the radio buttons
		self.qual_value.set ("Excellent")						# TODO: put these strings into self.stringsList rather than hard-coding them
		
		self.root_frame = Frame (self)							# everything in the window goes into this frame
		self.initial_focus = self.build_body (self.root_frame)	# construct all the widgets, lay them out etc
		self.root_frame.pack (padx = 5, pady = 5)				# put a margin around the root_frame
		
		# all the data goes here in self.record_data so that the main program can get to it later
		if (ed_rec == None):
			self.record_data = catalog_lib.record ()			# make a clean record because we're adding something to the list
		else:
			self.set_fields (ed_rec)							# copy things over from the given record because we're editing an entry
			self.record_data = ed_rec

		self.build_okcancel ()									# build the ok/cancel buttons on the bottom of the window

		self.grab_set ()										# take over keyboard input

		if not self.initial_focus:
			self.initial_focus = self

		self.protocol ("WM_DELETE_WINDOW", self.cancel)
		self.geometry ("+%d+%d" % (parent.winfo_rootx () + 50, parent.winfo_rooty () + 50))
		self.initial_focus.focus_set ()
		self.wait_window (self)
		return

	def build_body (self, master):
		self.body_frame = Frame (self)
		# name
		Label (self.body_frame, text = "Name:").grid (row = 0, column = 0)
		self.name_field = Entry (self.body_frame)
#		self.name_field_str = StringVar ()
		self.name_field ["textvariable"] = self.name_field_str
		self.name_field.grid (row = 0, column = 1)
		# size
		Label (self.body_frame, text = "Size:").grid (row = 1, column = 0)
		self.size_field = Entry (self.body_frame)
#		self.size_field_str = StringVar ()
		self.size_field ["textvariable"] = self.size_field_str
		self.size_field.grid (row = 1, column = 1)
		# censored/uncensored
#		self.cen_uncen = StringVar ()			# this is the var that holds the value of the radio buttons
#		self.cen_uncen.set ("Uncensored")
		self.cen_radio = Radiobutton (self.body_frame, text = "Censored", variable = self.cen_uncen, value = "Censored")
		self.cen_radio.grid (row = 2, column = 0)
		self.uncen_radio = Radiobutton (self.body_frame, text = "Uncensored", variable = self.cen_uncen, value = "Uncensored")
		self.uncen_radio.grid (row = 2, column = 1)
		# subs
		Label (self.body_frame, text = "Subs:").grid (row = 3, column = 0)
		self.subs_field = Entry (self.body_frame)
#		self.subs_field_str = StringVar ()
		self.subs_field ["textvariable"] = self.subs_field_str
		self.subs_field.grid (row = 3, column = 1)
		# dubs
		Label (self.body_frame, text = "Dubs:").grid (row = 4, column = 0)
		self.dubs_field = Entry (self.body_frame)
#		self.dubs_field_str = StringVar ()
		self.dubs_field ["textvariable"] = self.dubs_field_str
		self.dubs_field.grid (row = 4, column = 1)
		# format
		Label (self.body_frame, text = "Format:").grid (row = 5, column = 0)
		self.form_field = Entry (self.body_frame)
#		self.form_field_str = StringVar ()
		self.form_field ["textvariable"] = self.form_field_str
		self.form_field.grid (row = 5, column = 1)
		# quality
		Label (self.body_frame, text = "Quality:").grid (row = 6, column = 1)
#		self.qual_value = StringVar ()
		self.excel_rad = Radiobutton (self.body_frame, text = "Excellent", variable = self.qual_value, value = "Excellent")
		self.excel_rad.grid (row = 7, column = 0)
		self.good_rad = Radiobutton (self.body_frame, text = "Good", variable = self.qual_value, value = "Good")
		self.good_rad.grid (row = 7, column = 1)
		self.poor_rad = Radiobutton (self.body_frame, text = "Poor", variable = self.qual_value, value = "Poor")
		self.poor_rad.grid (row = 7, column = 2)
		# comments
		Label (self.body_frame, text = "Comments:").grid (row = 8, column = 0)
		self.comm_field = Entry (self.body_frame)
#		self.comm_field_str = StringVar ()
		self.comm_field ["textvariable"] = self.comm_field_str
		self.comm_field.grid (row = 8, column = 1)
		
		self.body_frame.pack (side = TOP)
		# return the widget that should have initial keyboard focus
		return self.name_field

	def build_okcancel (self):
		# add standard button box. override if you don't want the
		# standard buttons
		self.okcancel_box = Frame (self)

		self.ok_button = Button (self.okcancel_box, text = "OK", command = self.ok, default = ACTIVE)
		self.ok_button.pack (side = LEFT)
		self.cancel_button = Button (self.okcancel_box, text = "Cancel", command = self.cancel)
		self.cancel_button.pack (side = LEFT)

		self.bind ("<Return>", self.ok)
		self.bind ("<Escape>", self.cancel)

		self.okcancel_box.pack (side = BOTTOM)
		return
	
	def set_fields (self, rec):
		self.name_field_str.set (rec.nam)
		self.size_field_str.set (rec.ssize.stringify ())
		self.cen_uncen.set (rec.censored)
		self.subs_field_str.set (rec.subs)
		self.dubs_field_str.set (rec.dubs)
		self.form_field_str.set (rec.format)
		self.qual_value.set (rec.quality)
		self.comm_field_str.set (rec.comment)
		return

	def ok (self, event = None):
		self.withdraw ()
		self.update_idletasks ()
		
		self.record_data.nam = self.name_field_str.get ()
		self.record_data.ssize.load_string (self.size_field_str.get ())
		self.record_data.censored = self.cen_uncen.get ()
		self.record_data.subs = self.subs_field_str.get ()
		self.record_data.dubs = self.dubs_field_str.get ()
		self.record_data.format = self.form_field_str.get ()
		self.record_data.quality = self.qual_value.get ()
		self.record_data.comment = self.comm_field_str.get ()
		
		self.was_canceled = False
		self.parent.focus_set ()
		self.destroy ()
		return

	def cancel (self, event = None):
		# put focus back to the parent window
		self.was_canceled = True
		self.parent.focus_set ()
		self.destroy ()
		return

#
#	a dialog that will destroy any duplicate files in the current working directory
#
class kill_dups_dialog (Toplevel):
	def __init__ (self, parent, title = ""):
		Toplevel.__init__ (self, parent)
		self.transient (parent)
		
		self.title (title)
		self.parent = parent
		# variables to hold values from the widgets
		self.dupfiles = [] # list of duplicate files
		
		self.initial_focus = self.build_body ()					# construct all the widgets, lay them out etc
		self.build_okcancel ()									# build the ok/cancel buttons on the bottom of the window
		self.grab_set ()										# take over keyboard input
		
		if not self.initial_focus:
			self.initial_focus = self
		
		self.protocol ("WM_DELETE_WINDOW", self.ok)
		self.geometry ("+%d+%d" % (parent.winfo_rootx () + 50, parent.winfo_rooty () + 50))
		self.initial_focus.focus_set ()							# set focus to the widget that should have it
		self.wait_window (self)
		return
	
	def build_body (self):
		# display the current directory that will be scanned
		self.curr_dir_frame = Frame (self)
		self.curr_dir_frame.pack (side = TOP, expand = NO, fill = X)
		Label (self.curr_dir_frame, text = os.getcwd (), relief = RAISED).pack (side = TOP, expand = YES, fill = BOTH)
		# root frame for everything else
		self.root_frame = Frame (self)
		self.root_frame.pack (side = TOP, expand = NO)
		# display a "do" button-- once pushed it will build a list of all duplicate files
		self.find_button = Button (self.root_frame, text = "Find Duplicates", command = self.find_dups)
		#self.find_button.grid (row = 0, column = 0, sticky = W)
		self.find_button.pack (side = LEFT)
		# progress bar to let the user know we're working
		self.prog_bar = progress_bar (self.root_frame, "progress", 1, "horizontal", 0, 100, 400)
		self.prog_bar.update_progress (0)
		#self.prog_bar.frame.grid (row = 0, column = 1, sticky = W)
		self.prog_bar.frame.pack (side = RIGHT)
		# frame to hold the listbox and stuff that deals with what we do after finding duplicates
		self.list_frame = Frame (self)
		self.list_frame.pack (side = TOP, expand = YES, fill = BOTH)
		# display a listbox that will hold the list of duplicate files
		self.dupfile_listbox = scrolled_listbox (self.list_frame, width = 64, height = 16)
		self.dupfile_listbox.pack (side = TOP, expand = YES, fill = BOTH)
		# bind <delete> for listbox to remove/redeem a file from the list of dups
		self.dupfile_listbox.listbox.bind ("<Delete>", self.redeemEntry)
		# display a "redeem" button that will remove a selected file from the list of dups
		self.redeemButton = Button (self.root_frame, text = "Redeem", command = self.redeemEntry)
		self.redeemButton.pack (side = BOTTOM)
		# display a "delete" button-- once pushed it will destroy all the files listed
		self.progress_label = Label (self.list_frame, text = "")
		self.progress_label.pack (side = LEFT, expand = NO, fill = X)
		self.delete_button = Button (self.list_frame, text = "Delete Duplicates", command = self.del_dups)
		self.delete_button.pack (side = RIGHT)
		return self.find_button
	
	def build_okcancel (self):
		# add standard button box. override if you don't want the
		# standard buttons
		self.okcancel_box = Frame (self)

		self.ok_button = Button (self.okcancel_box, text = "OK", command = self.ok, default = ACTIVE)
		self.ok_button.pack (side = RIGHT)

		self.bind ("<Return>", self.ok)
		self.bind ("<Escape>", self.ok)

		self.okcancel_box.pack (side = BOTTOM, expand = YES, fill = X)
		return
	
	def redeemEntry (self, event = None):
		# get the index from the listbox
		if (event == None): # if event == None then we were hit by the removeButton callback
			selectionIndex = self.dupfile_listbox.listbox.index (ACTIVE)
		else: # if event != None then we were hit by the <Delete> callback
			selectionIndex = event.widget.index (ACTIVE)
		#print ("redeeming: " + str (selectionIndex) + " :: " + self.dupfiles [selectionIndex])
		# delete the entry from the list of duplicates (now it will not be deleted)
		self.dupfiles.pop (selectionIndex)
		# now refresh the listbox
		self.dupfile_listbox.listbox.delete (0, END)
		for each in self.dupfiles:
			self.dupfile_listbox.listbox.insert (END, each)
		return
	
	def find_dups (self):
		# count how many files are in the current directory and set the progress bar max to that number
		# find all duplicate files in the current directory
		# every time we check a file increment the progress bar
		# if we found a duplicate then add its filename to the list of duplicates
		# when we're all done then add all duplicate filenames to the listbox
		
		# filecmp provides filecmp.cmp() which we use to compare file contents
		import filecmp
	
		# get a list of all the files in the current directory
		dir_list = os.listdir (os.getcwd ())
		# run through the list and take out any directories- we don't want to try file comparisons on a directory!
		x = 0
		while (x < len (dir_list)):
			if (os.path.isdir (dir_list [x]) == True):
				dir_list.pop (x)
			x = x + 1
	
		# sort the dir_list alphabetically so that we retain/delete the files in order rather than deleting random copies
		dir_list.sort ()
		# set up the progress bar
		self.prog_bar.max = len (dir_list)
		self.prog_bar.update_progress (0)
		
		self.progress_label.config (text = "Finding duplicates... checking (" + str (len (dir_list)) + ") files")
		# compare every file now that we're left with only files 
		x = 0
		while (x < len (dir_list)):
			y = 0
			while (y < len (dir_list)):
				if (x != y):															# if we're not about to compare a file to itself then compare files
					item_one = os.stat (dir_list [x])
					item_two = os.stat (dir_list [y])
					if (item_one.st_size == item_two.st_size):							# only do a deep file compare if the file sizes aren't the same
						if (filecmp.cmp (dir_list [x], dir_list [y], False) == True):	# if the files are duplicates then delete one
							self.dupfiles.append (dir_list [y])							# add the deleted file onto the list of deleted files
							dir_list.pop (y)
							self.prog_bar.update_progress (self.prog_bar.value + 1)
						else:
							y = y + 1
					else:
						y = y + 1
				else:
					y = y + 1
			# now that we've compared each to everything else we remove each from dir_list so that we don't compare against it when it's some other file's turn to check
			dir_list.pop (x)
			self.prog_bar.update_progress (self.prog_bar.value + 1)
		
		self.progress_label.config (text = "Done.")		# let the user know we're done
		self.dupfile_listbox.listbox.delete (0, END)
		for each in self.dupfiles:
			self.dupfile_listbox.listbox.insert (END, each)
		return
	
	def del_dups (self):
		# run through the list of duplicate files and destroy them
		for each in self.dupfiles:
			os.remove (each)
		
		self.dupfile_listbox.listbox.delete (0, END)
		return
	
	def ok (self, event = None):
		self.withdraw ()
		self.update_idletasks ()
		
		self.was_canceled = False
		self.parent.focus_set ()
		self.destroy ()
		return

#
#	a dialog to set preferences
#
class prefs_dialog (Toplevel):
	def __init__ (self, parent, title = "", preferences = None):
		Toplevel.__init__ (self, parent)
		self.transient (parent)
		
		self.title (title)
		self.parent = parent
		
		# data
		self.auto_mount_dir = StringVar ()
		self.sort_on_add = IntVar ()
		self.sort_on_save = IntVar ()
		self.auto_load = IntVar ()
		self.show_splash = IntVar ()
		if (preferences != None):
			self.auto_mount_dir.set (preferences.get_auto_mount_dir ())
			self.sort_on_add.set (preferences.get_sort_on_add ())
			self.sort_on_save.set (preferences.get_sort_on_save ())
			self.auto_load.set (preferences.get_auto_load ())
			self.show_splash.set (preferences.get_print_splash ())
		
		self.initial_focus = self.build_body ()					# construct all the widgets, lay them out etc
		self.build_okcancel ()									# build the ok/cancel buttons on the bottom of the window
		self.grab_set ()										# take over keyboard input
		
		if not self.initial_focus:
			self.initial_focus = self
		
		self.protocol ("WM_DELETE_WINDOW", self.cancel)
		self.geometry ("+%d+%d" % (parent.winfo_rootx () + 50, parent.winfo_rooty () + 50))
		self.initial_focus.focus_set ()
		self.wait_window (self)
		return
	
	def build_body (self):
		# foundation
		self.root_frame = Frame (self)
		self.root_frame.pack (side = TOP, expand = YES, fill = BOTH)
		# what directory to mount on startup (blank to disable)
		self.auto_mount_frame = Frame (self.root_frame)
		self.auto_mount_frame.pack (side = TOP, expand = YES, fill = BOTH)
		Label (self.auto_mount_frame, text = "Auto-mount directory:").pack (side = LEFT)
		self.auto_mount_entry = Entry (self.auto_mount_frame)
		self.auto_mount_entry ['textvariable'] = self.auto_mount_dir
		self.auto_mount_entry.pack (side = LEFT)
		# should we automatically sort the list when adding a record?
		self.checks_frame = Frame (self.root_frame)
		self.checks_frame.pack (side = TOP, expand = YES, fill = BOTH)
		self.sort_on_add_chkbox = Checkbutton (self.checks_frame, text = "Sort when adding a record", variable = self.sort_on_add, offvalue = 0, onvalue = 1)
		self.sort_on_add_chkbox.grid (row = 0, column = 0, sticky = W)
		self.sort_on_save_chkbox = Checkbutton (self.checks_frame, text = "Sort when saving a record list", variable = self.sort_on_save, offvalue = 0, onvalue = 1)
		self.sort_on_save_chkbox.grid (row = 1, column = 0, sticky = W)
		self.auto_load_chkbox = Checkbutton (self.checks_frame, text = "Load the last opened list on startup", variable = self.auto_load, offvalue = 0, onvalue = 1)
		self.auto_load_chkbox.grid (row = 2, column = 0, sticky = W)
		self.show_splash_chkbox = Checkbutton (self.checks_frame, text = "Show branding/splash screen", variable = self.show_splash, offvalue = 0, onvalue = 1)
		self.show_splash_chkbox.grid (row = 3, column = 0, sticky = W)
		return self.auto_mount_entry
	
	def build_okcancel (self):
		# add standard button box. override if you don't want the
		# standard buttons
		self.okcancel_box = Frame (self)

		self.ok_button = Button (self.okcancel_box, text = "OK", command = self.ok, default = ACTIVE)
		self.ok_button.pack (side = LEFT)
		self.cancel_button = Button (self.okcancel_box, text = "Cancel", command = self.cancel)
		self.cancel_button.pack (side = LEFT)

		self.bind ("<Return>", self.ok)
		self.bind ("<Escape>", self.cancel)

		self.okcancel_box.pack (side = BOTTOM)
		return
	
	def ok (self, event = None):
		# don't execute this function unless the enter button was pressed on the OK button
		# that way we don't toggle something like a checkbox and exit before the user can
		# see what happened
		if ((event != None) and (event.widget.widgetName != "button")):
			return
		
		self.withdraw ()
		self.update_idletasks ()
		
		self.was_canceled = False
		self.parent.focus_set ()
		self.destroy ()
		return
	
	def cancel (self, event = None):
		self.was_canceled = True
		self.parent.focus_set ()
		self.destroy ()
		return

#
#	a dialog to scan the contents of the current directory into a list
#
class scan_dialog (Toplevel):
	def __init__ (self, parent, title = ""):
		Toplevel.__init__ (self, parent)
		self.transient (parent)
		
		self.title (title)
		self.parent = parent
		
		# data
		self.scanned_list = 0
				
		self.initial_focus = self.build_body ()					# construct all the widgets, lay them out etc
		self.build_okcancel ()									# build the ok/cancel buttons on the bottom of the window
		self.grab_set ()										# take over keyboard input
		
		if not self.initial_focus:
			self.initial_focus = self
		
		self.protocol ("WM_DELETE_WINDOW", self.cancel)			# hitting the cancel button kills us
		self.geometry ("+%d+%d" % (parent.winfo_rootx () + 50, parent.winfo_rooty () + 50))
		self.initial_focus.focus_set ()
		self.wait_window (self)
		return
	
	def build_body (self):
		# display the current directory that will be scanned
		self.curr_dir_frame = Frame (self)
		self.curr_dir_frame.pack (side = TOP, expand = NO, fill = X)
		Label (self.curr_dir_frame, text = os.getcwd (), relief = RAISED).pack (side = TOP, expand = YES, fill = BOTH)
		# root frame for everything else
		self.root_frame = Frame (self)
		self.root_frame.pack (side = TOP, expand = NO)
		# display a "do" button-- once pushed it will scan all the files in the current directory
		self.scan_button = Button (self.root_frame, text = "Scan Directory", command = self.scan_dir)
		#self.find_button.grid (row = 0, column = 0, sticky = W)
		self.scan_button.pack (side = LEFT)
		# progress bar to let the user know we're working
		self.prog_bar = progress_bar (self.root_frame, "progress", 1, "horizontal", 0, 100, 400)
		self.prog_bar.update_progress (0)
		self.prog_bar.frame.pack (side = RIGHT)
		# frame to hold the listbox and stuff that deals with what we do after finding duplicates
		self.list_frame = Frame (self)
		self.list_frame.pack (side = TOP, expand = YES, fill = BOTH)
		# details box for scanned records
		self.details_box = rec_detail_box (self.list_frame, border = 2, relief = SUNKEN, height = 8)
		self.details_box.pack (side = TOP, expand = YES, fill = BOTH)
		# listbox for scanned records
		# note: the reference to self.scanned_list is empty because it hasn't been instantiated yet
		#		we'll have to re-set this reference after scanning so that the listbox displays stuff
		self.rec_listbox = rec_list_box (self.list_frame, self.details_box, self.scanned_list)
		self.rec_listbox.pack (side = BOTTOM, expand = YES, fill = BOTH)
		# bind <delete> for listbox to remove/redeem a file from the list
		self.rec_listbox.rec_listbox.bind ("<Delete>", self.redeemEntry)
		# display a "redeem" button that will remove a selected file from the list
		self.removeButton = Button (self.root_frame, text = "Remove", command = self.redeemEntry)
		self.removeButton.pack (side = BOTTOM)
		
		return self.scan_button
	
	def build_okcancel (self):
		# add standard button box. override if you don't want the
		# standard buttons
		self.okcancel_box = Frame (self)

		self.ok_button = Button (self.okcancel_box, text = "OK", command = self.ok, default = ACTIVE)
		self.ok_button.pack (side = LEFT)
		self.cancel_button = Button (self.okcancel_box, text = "Cancel", command = self.cancel)
		self.cancel_button.pack (side = LEFT)

		self.bind ("<Return>", self.ok)
		self.bind ("<Escape>", self.cancel)

		self.okcancel_box.pack (side = BOTTOM)
		return
	
	def scan_dir (self, event = None):
		# get a list of the cwd
		dirListing = os.listdir (os.getcwd ())
		# sort it to keep things neat and tidy
		dirListing.sort ()
		# set the progressbar max to however many items we have
		self.prog_bar.max = len (dirListing)
		self.prog_bar.update_progress (0)	# start at 0% done
		
		# instatiate a record list so that we have something to add to
		self.scanned_list = catalog_lib.rec_list ()
		
		# for every item in the cwd
		#	suck in its metadata into a record
		#	add that record to the list
		#	progressbar++
		for eachItem in dirListing:
			self.scanned_list.add (HC_sys.scan_item (eachItem))
			self.prog_bar.update_progress (self.prog_bar.value + 1)
		
		# give the scanned list to self.rec_listbox so that it can display what we just scanned
		self.rec_listbox.r_list_handle = self.scanned_list
		self.rec_listbox.refresh () # now show it
		return
	
	def redeemEntry (self, event = None):
		# get the index from the listbox
		if (event == None): # if event == None then we were hit by the removeButton callback
			selectionIndex = self.rec_listbox.rec_listbox.index (ACTIVE)
		else: # if event != None then we were hit by the <Delete> callback
			selectionIndex = event.widget.index (ACTIVE)
		
		# delete the entry from the scanned list and the listbox
		self.rec_listbox.delete (selectionIndex)
		return
	
	def ok (self, event = None):
		# don't execute this function unless the enter button was pressed on the OK button
		# that way we don't toggle something like a checkbox and exit before the user can
		# see what happened
		if ((event != None) and (event.widget.widgetName != "button")):
			return
		
		self.withdraw ()
		self.update_idletasks ()
		
		self.was_canceled = False
		self.parent.focus_set ()
		self.destroy ()
		return
	
	def cancel (self, event = None):
		self.was_canceled = True
		self.parent.focus_set ()
		self.destroy ()
		return

#
#	Display basic info about the program etc.
#
class about_dialog (Toplevel):
	def __init__ (self, parent, title = "About"):
		Toplevel.__init__ (self, parent)
		self.transient (parent)
		
		self.title (title)
		self.parent = parent
				
		self.initial_focus = self.build_body ()					# construct all the widgets, lay them out etc
		self.build_okcancel ()									# build the ok/cancel buttons on the bottom of the window
		self.grab_set ()										# take over keyboard input
		
		if not self.initial_focus:
			self.initial_focus = self
		
		self.protocol ("WM_DELETE_WINDOW", self.ok)
		self.geometry ("+%d+%d" % (parent.winfo_rootx () + 50, parent.winfo_rooty () + 50))
		self.initial_focus.focus_set ()
		self.wait_window (self)
		return
	
	def build_body (self):
		self.info_frame = Frame (self)
		self.info_frame.pack (side = TOP, expand = YES, fill = BOTH)
		# show the stuff
		Label (self.info_frame, text = "Written by: DoctorMe").pack (side = TOP, expand = YES, fill = BOTH)
		Label (self.info_frame, text = "Version: " + HC_sys.HC_version_string).pack (side = TOP, expand = YES, fill = BOTH)
		Label (self.info_frame, text = "For help, look into \"../doc/README.txt\" and \"../doc/INSTRUCTIONS.txt\"").pack (side = TOP, expand = YES, fill = BOTH)
		return
	
	def build_okcancel (self):
		self.okcancel_box = Frame (self)

		self.ok_button = Button (self.okcancel_box, text = "OK", command = self.ok, default = ACTIVE)
		self.ok_button.pack (side = LEFT)

		self.bind ("<Return>", self.ok)
		self.bind ("<Escape>", self.ok)

		self.okcancel_box.pack (side = BOTTOM)
		return
	
	def ok (self, event = None):		
		self.withdraw ()
		self.update_idletasks ()
		
		self.was_canceled = False
		self.parent.focus_set ()
		self.destroy ()
		return

