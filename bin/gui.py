#
#	this file contains everything necessary for the GUI interface (PyGTK)
#

from Tkinter import *
import tkFileDialog
import os
import sys
# our own modules
import catalog_lib
import HC_sys
import prefs_lib
import gui_lib

class hc_gui ():
	def __init__ (self):
		# the record list
		self.rec_lizt = catalog_lib.rec_list ()
		# the preferences
		self.prefs = prefs_lib.preferences ()
		
		# GUI STUFF \/\/\/\/\/\/
		self.master_window = Tk ()
		self.master_window.title ("Cataloger")
		self.master_window.geometry ("300x200-30+30")
		
		# main form
		self.m_form = Frame (self.master_window)
		self.m_form.pack (expand = YES, fill = BOTH)
		
		# list box for the records
		self.records_frame = Frame (self.m_form, border = 2, relief = RAISED)	# hold everything to do with the records interface (rec_listboxes, toolbar, etc)
		self.records_frame.pack (side = LEFT, expand = YES, fill = BOTH)		# fill up our half of the main window
		Label (self.records_frame, text = "Current Record List").pack (side = TOP)
		
		self.rec_button_fr = Frame (self.records_frame)							# frame to hold the buttons for dealing with records (toolbar)
		self.rec_button_fr.pack (side = TOP, expand = NO, fill = X)				# put it above the list boxes, but don't let it expand vertically
		
		# a box to display details about the currently selected record
		self.detailbox = gui_lib.rec_detail_box (self.records_frame, border = 2, relief = SUNKEN, height = 8) # this will display the details of whatever record is selected in self.rec_listbox
		self.detailbox.pack (side = TOP, expand = YES, fill = BOTH)				# give it enough space down below to show what we need
		# record listbox and scroll bar
		# this widget will automatically handle relevant tasks for self.rec_lizt
		self.rec_listbox = gui_lib.rec_list_box (self.records_frame, self.detailbox, self.rec_lizt)
		self.rec_listbox.pack (side = BOTTOM, expand = YES, fill = BOTH)
		
		self.add_rec_button = Button (self.rec_button_fr, text = "Add", command = self.add_rec_dialog) # a quick button to add a record (in case they don't want to use the menu entry)
		self.add_rec_button.pack (side = LEFT)									# put it in the toolbar
		self.edit_rec_button = Button (self.rec_button_fr, text = "Edit", command = self.edit_rec_dialog)
		self.edit_rec_button.pack (side = LEFT)
		self.del_rec_button = Button (self.rec_button_fr, text = "Delete", command = self.rec_listbox.onDelete)
		self.del_rec_button.pack (side = LEFT)
		
		# address bar
		self.directory_frame = Frame (self.m_form, border = 2, relief = RAISED)
		self.directory_frame.pack (side = RIGHT, expand = YES, fill = BOTH)
		self.adr_bar = Frame (self.directory_frame)
		self.adr_bar.pack (side = TOP, expand = NO, fill = X)
		Label (self.adr_bar, text = "File View").pack (side = TOP)
		self.adr_goUp = Button (self.adr_bar, text = "UP", command = self.dir_goUp)
		self.adr_goUp.pack (side = LEFT)
		self.adr_goTo = Button (self.adr_bar, text = "GO", command = self.change_dir)
		self.adr_goTo.pack (side = LEFT)
		self.adr_dirPathbox = Entry (self.adr_bar)
		self.adr_dirPathbox.text_field = StringVar ()							# construct a place for entered text to go
		self.adr_dirPathbox ['textvariable'] = self.adr_dirPathbox.text_field	# connect that place with the text that's typed into the widget
		self.adr_dirPathbox.bind ("<Return>", self.dir_box_onReturn)
		self.adr_dirPathbox.pack (side = RIGHT, expand = YES, fill = X)
		# list box and scroll bar for the current directory
		self.dir_listbox = Listbox (self.directory_frame, height = 32)
		self.dir_listbox.bind ("<Double-1>", self.dir_list_onDclick)
		self.dirlistbox_scrlbar = Scrollbar (self.directory_frame, command = self.dir_listbox.yview, orient = VERTICAL)
		self.dir_listbox.configure (yscrollcommand = self.dirlistbox_scrlbar.set)
		self.dirlistbox_scrlbar.pack (side = RIGHT, expand = NO, fill = Y)
		self.dir_listbox.pack (side = RIGHT, expand = YES, fill = BOTH)
		
		# main menu
		self.menu_bar = Menu (self.master_window)
		# main menu >> File
		self.file_menu = Menu (self.menu_bar, tearoff = 0)
		self.file_menu.add_command (label = "Open", command = self.open_file)
		self.file_menu.add_command (label = "Save", command = self.save_file)
		self.file_menu.add_command (label = "Concatenate", command = self.concat_file)
		self.file_menu.add_command (label = "Append", command = self.append_file)
		self.file_menu.add_separator ()
		self.file_menu.add_command (label = "Preferences", command = self.doPrefsDialog)
		self.file_menu.add_separator ()
		self.file_menu.add_command (label = "Exit", command = self.terminate)
		self.menu_bar.add_cascade (label = "File", menu = self.file_menu)
		# main menu >> List
		self.list_menu = Menu (self.menu_bar, tearoff = 0)
		self.list_menu.add_command (label = "Add", command = self.add_rec_dialog)
		self.list_menu.add_command (label = "Edit", command = self.edit_rec_dialog)
		self.list_menu.add_command (label = "Delete", command = self.rec_listbox.onDelete)
		self.list_menu.add_command (label = "Sort by name", command = lambda: self.sort_rec_list (0))
		self.list_menu.add_command (label = "Sort by size", command = lambda: self.sort_rec_list (1))
		self.menu_bar.add_cascade (label = "List", menu = self.list_menu)
		# main menu >> Tools
		self.tools_menu = Menu (self.menu_bar, tearoff = 0)
		self.tools_menu.add_command (label = "Batch Rename", command = self.batch_rename_dialog)
		self.tools_menu.add_command (label = "Delete Duplicates", command = self.del_dups_dialog)
		self.tools_menu.add_command (label = "Scan Directory", command = self.scan_dir_dialog)
		self.menu_bar.add_cascade (label = "Tools", menu = self.tools_menu)
		# main menu >> Help
		self.help_menu = Menu (self.menu_bar, tearoff = 0)
		self.help_menu.add_command (label = "About", command = self.help_dialog)
		self.menu_bar.add_cascade (label = "Help", menu = self.help_menu)
		
		self.master_window.config (menu = self.menu_bar)
		
		# go to the auto_mount_dir if it's been set
		if (self.prefs.get_auto_mount_dir () != ""):
			self.change_dir (self.prefs.get_auto_mount_dir ()) # if it's been set then go there
		else:
			self.change_dir (sys.path [0]) # otherwise, mount the directory in which the program was started
		
		# if we've been told to load the last opened file then load it (if it exists)
		if ((self.prefs.get_auto_load () == True) and (self.prefs.get_last_five () [0] != "")):
			if (sys.path.exists (self.prefs.get_last_five () [0]) == True):
				self.rec_lizt.load_file (self.prefs.get_last_five () [0])
				self.rec_listbox.refresh ()
		
		self.master_window.mainloop ()
		return
	
	def terminate (self):
		#TODO # save if it's set in the prefs
		
		# terminate
		self.master_window.quit ()
		return
	
	def batch_rename_dialog (self):
		# do the rename
		self.dlg = gui_lib.rename_dialog (self.master_window, "Batch Rename")
		if (self.dlg.was_canceled == False):
			# refresh the listing of the current directory
			self.dir_list = os.listdir (os.getcwd ())
			self.dir_list.sort ()
			# clear out the list of files in the current dir
			self.dir_listbox.delete (0, END)
			self.dir_listbox.insert (END, "..")		# entry to go up one
			for each in self.dir_list:
				self.dir_listbox.insert (END, each)
		return
	
	def dir_goUp (self):
		""" go up one from the current working directory """
		self.change_dir ("..")
		return
	
	def dir_box_onReturn (self, event):
		self.change_dir (self.adr_dirPathbox.text_field.get ())
		return "break"
	
	def dir_list_onDclick (self, event):
		if (len (event.widget.curselection ()) > 0):
			self.dir_indx = event.widget.curselection () [0]	# find out which index was double clicked on
			self.change_dir (event.widget.get (self.dir_indx))	# get the text from that index and pass it to self.change_dir()
		return "break"										# return "break" to prevent Tkinter's built-in callbacks from doing things
	
	def change_dir (self, new_dir = ""):
		"""
		go to the given path if it is a directory and then update the directory listbox
		"""
		self.dir_was_changed = False
		if (new_dir == ""):
			if ((os.path.exists (self.adr_dirPathbox.text_field.get ()) == True) and (os.path.isdir (new_dir) == True)):
				os.chdir (self.adr_dirPathbox.text_field.get ())
				self.dir_was_changed = True
		else:
			if ((os.path.exists (new_dir) == True) and (os.path.isdir (new_dir) == True)):
				os.chdir (new_dir)
				self.dir_was_changed = True
				
		# set this string here so we don't have to keep calling getcwd()
		newDirStr = os.getcwd ()
		
		if (self.dir_was_changed == True): # only bother to do the following work if the current directory was actually changed
			# set the pathbox so that it displays the current directory	
			self.adr_dirPathbox.text_field.set (newDirStr)
		
			# refresh the listing of the current directory
			self.dirListing = os.listdir (newDirStr)
			
			# separate directories from files
			# this isn't as slow and memory hoggy as it first appears because items are being passed around
			# by reference-- copies aren't actually being made
			self.dir_list = []
			self.file_list = []
			x = 0
			for eachItem in self.dirListing:
				if (os.path.isdir (newDirStr + HC_sys.dir_divider + eachItem)):	# if it's a directory then
					self.dir_list.append (eachItem)								# pack it into the list of directories
				else:
					self.file_list.append (eachItem)							# otherwise it's a file
				x = x + 1
			
			self.dir_list.sort ()					# sort both lists alphabetically
			self.file_list.sort ()
			
			# fill in the directory listbox with the directories and then the files
			self.dir_listbox.delete (0, END)
			self.dir_listbox.insert (END, "..")		# entry to go up one
			for each in self.dir_list:				# insert all the directories
				self.dir_listbox.insert (END, each)
			for each in self.file_list:				# insert all the files
				self.dir_listbox.insert (END, each)
		return
	
	def open_file (self):
		# pop up a file dialog to find the .lzt file
		self.op_file = tkFileDialog.askopenfile (parent = self.master_window, initialdir = HC_sys.SAVES_DIR, mode = 'r', title = 'Choose a file')
		if (self.op_file != None):
			# load the .lzt file
			self.rec_lizt.load_file (self.op_file.name)
			# display it in the record panel
			self.rec_listbox.refresh ()
		return
	
	def save_file (self):
		# pop up a file dialog to find the .lzt file
		self.op_file = tkFileDialog.asksaveasfile (parent = self.master_window, initialdir = HC_sys.SAVES_DIR, mode = 'w', title = 'Choose a file')
		if (self.op_file != None):
			# sort before saving if it's set in the prefs
			if (self.prefs.get_sort_on_save () == True):
				self.rec_lizt.sort_list ("name")
				self.rec_listbox.refresh ()
			# save the .lzt file
			self.rec_lizt.save_file (self.op_file.name)
		return
	
	def concat_file (self):
		self.cat_file = tkFileDialog.askopenfile (parent = self.master_window, initialdir = HC_sys.SAVES_DIR, mode = 'r', title = "Choose a file")
		if (self.cat_file != None):
			self.rec_lizt.concat_file (self.cat_file.name)
			self.rec_listbox.refresh ()
		return
	
	def append_file (self):
		self.app_file = tkFileDialog.asksaveasfile (parent = self.master_window, initialdir = HC_sys.SAVES_DIR, mode = 'w', title = "Choose a file")
		if (self.app_file != None):
			self.rec_lizt.append_to_file (self.app_file.name)
			self.rec_listbox.refresh ()
		return
	
	def add_rec_dialog (self):
		self.dlg = gui_lib.add_rec_dialog (self.master_window, "Add a record")
		if (self.dlg.was_canceled == False and self.dlg.record_data.nam != ""): # if it was cancelled then don't bother adding the data
			self.rec_lizt.add (self.dlg.record_data)
			self.rec_listbox.refresh ()
		return
	
	def edit_rec_dialog (self, event = None):
		if (event == None): # if None then we were called via button
			self.ed_index = self.rec_listbox.rec_listbox.index (ACTIVE)
		else: # otherwise we can get the index from the widget that was hit in the event
			self.ed_index = event.widget.rec_listbox.index (ACTIVE)
		
		if (self.rec_lizt.index_valid (self.ed_index) == True):
			self.selected_rec = self.rec_lizt.lizt [self.ed_index]
			self.dlg = gui_lib.add_rec_dialog (self.master_window, "Edit a record", self.selected_rec)
		# if the user hits OK then update the list
		if (self.dlg.was_canceled == False):
			self.rec_listbox.refresh ()
		return
	
	def del_dups_dialog (self):
		self.dlg = gui_lib.kill_dups_dialog (self.master_window, "Delete duplicate files")
		return
	
	def doPrefsDialog (self, event = None):
		self.dlg = gui_lib.prefs_dialog (self.master_window, "Preferences", self.prefs)
		if (self.dlg.was_canceled == False):
			self.prefs.set_auto_mount_dir (str (self.dlg.auto_mount_dir.get ()))
			self.prefs.set_sort_on_add (bool (self.dlg.sort_on_add.get ()))
			self.prefs.set_sort_on_save (bool (self.dlg.sort_on_save.get ()))
			self.prefs.set_auto_load (bool (self.dlg.auto_load.get ()))
			self.prefs.set_print_splash (bool (self.dlg.show_splash.get ()))
			self.prefs.save ()
		return
	
	def scan_dir_dialog (self, event = None):
		self.dlg = gui_lib.scan_dialog (self.master_window, "Scan Directory")
		if (self.dlg.was_canceled == False):
			self.rec_lizt.append_list (self.dlg.scanned_list)
			self.rec_listbox.refresh ()
		return
	
	def help_dialog (self, event = None):
		self.dlg = gui_lib.about_dialog (self.master_window)
		return
	
	def sort_rec_list (self, mode):
		if (mode == 0):
			self.rec_lizt.sort_list ("name")
		elif (mode == 1):
			self.rec_lizt.sort_list ("size")
		self.rec_listbox.refresh ()
		return
	
	def refresh_listbox (self, the_box, element_list):
		the_box.delete (0, END)
		for each in element_list:
			the_box.insert (END, each)
		return


#
#	here we actually run the GUI-- this is called from ../main.py
#
def gui_main ():
	app = hc_gui ()
	return

