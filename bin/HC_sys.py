#
#	this file contains global variables and functions that deal with the operating system etc.
#
import os

HC_version_string = "Cataloger v5.0"

sep_str_dl = "---------------------------------------------------------------------------------------------------------------------------"
sep_str_eq = "==========================================================================================================================="

# this is the location of the main.py file -- used to figure out where the .cfg file and ./bin and ./saves directories are
progDir = ""
# this is set on startup to match the conventions of the host OS
dir_divider = "/"
# this is the path to save .lzt files to
SAVES_DIR = ""

def get_prompt ():
	return os.getcwd () + prompt_string

def cwd ():
	return os.getcwd ()

def ls_dir (some_dir = ""):
	"""	print out the listing of a directory in a nicely readable format """
	if (some_dir == ""):
		abs_path = False			# we're working relative to the current dir
	else:
		local_dir = os.getcwd ()	# store this so that we can come back to it when we're done
		os.chdir (some_dir)			# go to the specified dir
		abs_path = True				# we're working from a specific directory
	
	dir_list = os.listdir (os.getcwd ())
	dir_list.sort ()
	
	for each in dir_list:
		item = os.stat (each)
		sze = item.st_size / 1024.0
		if (os.path.isdir (os.getcwd () + dir_divider + each)):
			dflag = '<directory>'
		else:
			dflag = 'KB'
		
		spacer_one = ' ' * int (8 - len (str ( int (sze))))
		spacer_two = ' ' * int (16 - len (dflag))
		#should spit out size of each file in kilobytes and the name of the file
		print "	%(size).1f" % {'size': sze} + spacer_one + "%(D)s" % {'D': dflag} + spacer_two + "%(E)s" % {'E': each}
		
	if (abs_path == True):
		os.chdir (local_dir) # go back to where we were
	return

def change_dir (dir_str):
	""" change the os.getcwd () to the appropriate absolute path """
	rtrn = True
	if (os.path.exists (dir_str)): # if it's an absolute path or a valid path then go to it
		os.chdir (dir_str)
	elif (os.path.exists (os.getcwd () + dir_divider + dir_str) == True): # if it's a relative path and it exists then go
		os.chdir (dir_str)
	else: # otherwise it ain't valid so we'll just sit where we are
		rtrn = False
	return rtrn

def scan_cwd ():
	""" scan the current directory and build a skeleton list of all the files in it: name, size, format """
	import catalog_lib
	rtrn = catalog_lib.rec_list ()
	dir_list = os.listdir (os.getcwd ())		# build a list of all filenames in the current directory
	for x in dir_list:
		item = os.stat (x)
		temp_size = item.st_size
		if (os.path.isdir (x) == False):		# if it's not a directory then it's a file
			temp_format = x [x.rfind ("."):]
			temp_nam = x [0:x.rfind (".")]		# chop off the file extension
		else:
			temp_format = "<DIR>"
			temp_nam = x
		temp_rec = catalog_lib.record (temp_nam, temp_size, "", "", "", temp_format, "", "")
		rtrn.add (temp_rec)
	return rtrn
	
def scan_item (itemFilename):
	""" suck in all the metadata about an item (file) and return it as a record suitable to be added to the list """
	# this is used in gui_lib.scan_dialog because we update the progress bar every time a new file is scanned
	
	import catalog_lib
	
	itemMetaData = os.stat (itemFilename)
	itemSize = itemMetaData.st_size
	if (os.path.isdir (itemFilename)):				# if it's a directory then it has no file extension
		itemFormat = "<DIR>"
		itemName = itemFilename
	else:											# if it's a file then we don't want the extension in the item name
		extPeriodIndex = itemFilename.rfind (".")	# gimme the index of the rightmost '.' in the filename
		itemFormat = itemFilename [extPeriodIndex:]	# everything to the right of the rightmost '.' is the file extension/format
		itemName = itemFilename [0:extPeriodIndex]	# everything to the left of the rightmost '.' is the item name
	
	# name, size, cen, subs, dubs, format, quality, comments
	rtrnRecord = catalog_lib.record (itemName, itemSize, "", "", "", itemFormat, "", "")
	return rtrnRecord

def batch_rename (name_str = "", paddingAmount = 1):
	""" rename all files in the current directory """
	# count how many items are to be renamed so that we have enough digits to cover it
	# (40,000 items can't be done with only 3 digits...)
	dir_list = os.listdir (os.getcwd ())
	dir_list.sort ()
	
	itemCount = 0
	for eachItem in dir_list:
		itemCount = itemCount + 1
	
	# if we don't have enough padding digits then make 'em
	if (pow (10, paddingAmount) < itemCount):
		paddingAmount = len (str (itemCount))
	
	# now let's zip back through and rename them
	x = 0
	for each_file in dir_list:
		if (os.path.isdir (each_file) == False): # only rename it with file extension if it's a file
			ext_str = "." + each_file.rsplit (".") [1] # grab the extension from the original filename
			os.rename (each_file, (new_filename + str (x).zfill (paddingAmount)) + ext_str)
			x = x + 1
	return

def batch_tag (tagString):
	dirList = os.listdir (os.getcwd ())
	dirList.sort ()
	for eachFile in dirList:
		if (os.path.isdir (eachFile) == False):
			# stringHalves [0] = the file name
			# stringHalves [1] = the file extension
			stringHalves = eachFile.rsplit (".")
			os.rename (eachFile, stringHalves [0] + tagString + "." + stringHalves [1])
		else:
			# if not a file then just tack the tags onto the end
			os.rename (eachFile, (eachFile + tagString))
	return

def kill_dup_files ():
	"""
	returns a list of deleted files
	compare every file in the current directory to every other file in the
	current directory. if we find any duplicates then we delete 'em
	"""
	# filecmp provides filecmp.cmp() which we use to compare file contents
	import filecmp
	
	rtrn = [] # list of deleted files
	
	# get a list of all the files in the current directory
	dir_list = os.listdir (os.getcwd ())
	
	# run through the list and take out any directories- we don't want to try file comparisons on a directory!
	x = 0
	while (x < len (dir_list)):
		if (os.path.isdir (dir_list [x]) == True):
			dir_list.pop (x)
		x = x + 1
	
	# sort the dir_list alphabetically so that we retain/delete the files in order rather than deleting random copies
	dir_list.sort ()
		
	# compare every file now that we're left with only files 
	x = 0
	while (x < len (dir_list)):
		y = 0
		while (y < len (dir_list)):
			if (dir_list [x] != dir_list [y]):									# if we're not about to compare a file to itself then compare files
				if (filecmp.cmp (dir_list [x], dir_list [y], False) == True):	# if the files are duplicates then delete one
					rtrn.append (dir_list [y])									# add the deleted file onto the list of deleted files
					os.remove (dir_list [y])
					dir_list.pop (y)
				else:
					y = y + 1
			else:
				y = y + 1
		# now that we've compared each to everything else we remove each from dir_list so that we don't compare against it when it's some other file's turn to check
		dir_list.pop (x)
		#x = x + 1
	return rtrn

