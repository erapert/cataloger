#!/usr/bin/python
#
#	This file contains only what's needed to launch the program in the chosen mode (gui or cli)
#
def main ():
	#setting it up so that we can import from a sub-directory:
	import sys
	import os
	
	# chop off "/main.py" from argv[0]: this gives us the starting directory,
	# we'll remember this because ./bin, ./saves, and the .cfg file are all there
	myProgDir = os.getcwd ()
	
	# working around platform specific crap
	if (sys.platform == 'win32'):
		# for Windows:
		sys.path.append (myProgDir + "\\bin")
		main_SAVES_DIR = myProgDir + "\\saves\\"
		dd = "\\"
	else:
		# for Linux: (and everyone else-- windows is the only one that uses '\' for directory separators)
		sys.path.append (myProgDir + "/bin/")
		main_SAVES_DIR = myProgDir + "/saves/"
		dd = "/"
	
	import HC_sys
	#HC_sys.curr_dir_str = os.getcwd ()
	HC_sys.SAVES_DIR = main_SAVES_DIR
	HC_sys.progDir = myProgDir
	HC_sys.dir_divider = dd

	# if arguments are given then we'll run in cli mode
	if (len (sys.argv) > 1):
		import cli
		cli.cli_main ()
	else: # default to graphical interface if no arguments are given
		import gui
		gui.gui_main ()
	return

# this should be commented out if using Stand Alone Python because launch_c.exe will automatically call main()
# so if this is uncommented then main() will be called twice
main ()

