###########################################################################################################
#                                                                                                         #
#          #######                     ##                ##                                  #####        #
#       ############                  ###                ###                              ###########     #
#     ################               ####                ####                           ###############   #
#   ######      #######              ####                ####                         ######      ######  #
#  #####           ####     ##########################   ####                        #####           ###  #
#  ####                  ##########################      ####                        ####                 #
#  ####                    ####      ####      ####      ####          #########     ####                 #
#  ####                  ########    ####    ########    ####        #############   ####                 #
#  ####                 ####  ####   ####   ####  ####   ####       ####       ####  ####                 #
#  ####                ####    ####  ####  ####    ####  ####      ####         #### ####      #########  #
#  ####            ### ###      ###  ####  ###      ###  ####      ####         #### ####       ########  #
#  #####          #### ############  ####  ############  ####      ####         #### #####         #####  #
#   #######     #####  ####    ####  ####  ####    ####  ####       ####       ####   #######    #######  #
#    ###############   ###      ###  ####  ###      ###  ##########  #############     #################  #
#      ###########     ###      ###  ####  ###      ###  ##########    #########         ###########  ##  #
#                                                                                                         #
###########################################################################################################
							
									INSTRUCTIONS

CONTENTS:
	Program Dependancies
	Installing the Program
	Starting the Program
	The Graphical User Interface
	Main GUI Dialog Windows
	The Command Line Interface
	Preferences/.cfg
	FAQ

PROGRAM DEPENDANCIES:
	2.X series Python (i.e. 2.7)
	A terminal of some sort for the command line interface (every system has one)
	Tkinter for the GUI (usually installed along with Python on Windows)

INSTALLING THE PROGRAM:
	Just extract the program to a convenient location on your hard drive. I recommend that you extract to
		your home directory (i.e. ~/catalogger).
	
	If you don't have Python installed on your system already then you'll need to install it (obviously).
		If you're too lazy to google it then use this link for the Windows installer:
			http://www.python.org/ftp/python/2.6.4/python-2.6.4.msi
		If you're on Linux then you should already have Python installed. If  your system has some retarded
		configuration and doesn't have Python then you might try one of the following commands in a terminal
		(use sudo or as root):
			Ubuntu/Debian:	apt-get install python
			Fedora/rpm:		yum install python
			Arch:			pacman -S python
	
	If you're on Windows then Tkinter should have been installed when you installed Python. If you're on
		Linux then you may need to install Tkinter. Try the following commands (or a graphical packagemanager):
			Ubuntu/Debian:	apt-get install python-tk
			Fedora/rpm:		yum install tkinter
			Arch:			pacman -S tk
	
STARTING THE PROGRAM:
	To run the program you need to run "../main.py".
	Windows:
		Either double click main.py or open a command line then navigate to it and run it.
	Linux:
		Double clicking main.py may work depending on how your system is set up. Otherwise you'll need to
		navigate to it in a terminal and run it from there. Remember to set execute permissions!
		(Try something like: chmod ugo+rx main.py)
	
	Note: for CLI mode you may want to set your terminal window to a size larger than the default 80x25

GRAPHICAL USER INTERFACE:
	Brand new in v4.5 is the GUI. It's written with Python's Tkinter module (a.k.a. the Tk toolkit). It provides
		the same functionality as the Command Line Interface, just with fancy buttons.
	
	On the very top of the window you'll see a menu bar that has the following menus in it:
		File:
			Open = Open a .lzt file
			Save = Save a .lzt file
			Concatenate = Concatenate a .lzt file onto the end of the currently loaded record list
			Append = Append the currently loaded record list onto the end of a .lzt file which is on the hard drive
			Preferences = View and edit program preferences (much nicer than editing a .cfg file by hand, eh?)
			Exit = exits the program, obviously
		Edit:
			Add = Add a record to the currently loaded list. If sort_on_add is true then the list will be sorted
			Edit = Edit the selected entry in the record list
			Delete = Delete the selected entry in the record list
		List:
			Sort by name = Sort the currently loaded record list by name
			Sort by size = Sort the currently loaded record list by size
		Tools:
			Batch Rename = Open the batch renamer dialog operating in the current working directory
			Delete Duplicates = Open the delete duplicates dialog in the current working directory
			Scan Directory = Open the scan directory dialog in the current working directory
		Help:
			About = Show an "About" dialog giving me credit, showing version string, and pointing to ../doc
	
	There are two main panes in the main window:
		Current Record List: on the left, displays the currently loaded record list along with some buttons for quick access
			to Add, Edit, and Delete. These three buttons operate only on the record list (NOT on files on the
			hard drive).
			
			Right under the Add, Edit, and Delete buttons is a little box which displays details about whatever record
				is selected in the listbox below it. Select a record by single clicking on it or by using the UP and DOWN
				arrow keys on the keyboard
	
		File View: on the right, a list of all files and directories in the current directory. Above that list you'll
			see an address bar with an "UP" and a "GO" button. The "UP" button moves up one directory level. The
			"GO" button will navigate directly to whatever address is in the address bar. You can also double click
			on any directory in the list to go into it (or double click on the ".." entry to go up one level).
	
	THE MAIN DIALOG WINDOWS:
		Add a record:
			You'll be presented with a window that contains various fields for you to fill out. The only field that
			is required is the "Name" field. If you don't enter a name for the new record then it won't be added to the list.
			Press "OK" to add the record, or "Cancel" if you decide to bail.
		
		Edit a record:
			Same thing as the "Add a record" dialog except that it's pre-filled with the data from the record that you
			had selected. Pressing "OK" will apply the changes.
		
		Batch Rename:
			The current working directory will be displayed at the top of the window. Be sure you check this directory
			because once you apply the function there's no way to undo it. The various fields in the window are, for the
			most part, optional. But you'll definately want to set the number of padding digits high enough for the number
			of files in the current working directory (if you have, say, 2000 files you'll want to set the number of
			padding digits to at least 4). The checkbox which says "Replace spaces with underscores" will work on whatever
			you typed into the "Name" field at the top of the window. All the radio buttons and checkboxes under the "Tags"
			heading just append a tag to the name that you put in the "Name" field (i.e. "[ENG_SUBS]"). After pushing "OK"
			every file in the current working directory will be renamed according to the following pattern:
				<NAME> + <CENSORED_TAG> + <SUBS_TAG> + <DUBS_TAG> + <SEQUENTIAL_ZERO_PADDED_NUMBER> + <FILE_EXTENSION>
			For example, if I had a directory of 5 .jpg files and I set the "Name" field to "something something",
			and hit the "Uncensored" tag, set the number convention to "[###]", number of padding digits set to 3, and
			checked the "Replace spaces with underscores" then I'd get the following:
				something_something_[UNCEN]_[000].jpg
				something_something_[UNCEN]_[001].jpg
				something_something_[UNCEN]_[002].jpg
				something_something_[UNCEN]_[003].jpg
				something_something_[UNCEN]_[004].jpg
		
		Delete duplicate files:
			The current working directory is displayed at the top of the window. Double check this.
			Right under that is a button that says "Find Duplicates" and just to the right of that is a progress bar.
			Hit "Find Duplicates" to build a list of all the dupicate files found in the current working directory.
			The progress bar should fill up with each file scanned. This process may take a long time depending on
			how many files are in the directory. Once it's finished scanning you'll see a list of files in the listbox
			under the "Find Dupicates" button. To delete all the files in the listbox simply press the "Delete Duplicates"
			button. Nothing will be deleted unless you press the "Delete Dupicates" button. To exit the window hit "OK".
			On the bottom left, just under the listbox, you'll see textual feedback of how the scan is going. When it says
			"Done." you'll know that the scan is completed.
		
		Scan Directory:
			The current working directory is displayed at the top of the window. Double check this.
			Right under that is a button that says "Scan Directory" and just to he right of that is a progress bar.
			The progress bar should fill up with each file scanned. This process shouldn't take much time.
			Below is a detail box and a listbox just like in the main window. You can use these to inspect the records
			that the scan function brought in. To add everything in the listbox to the currently loaded record list just
			hit the "OK" button. Hit "Cancel" to abort.
			

COMMAND LINE INTERFACE:
	Not all systems have access to a GUI environment-- say, if you're running a minimalist home server with
		no X environment and you access it strictly through ssh via PuTTY-- you may still want or need some
		of the tools available with this program.
	
	To run the cataloger without the GUI simply pass arguments to it when you run it. Multiple arguments can
		be specified and they will be executed in the order that they were passed in.
		
		i.e. ./main.py -h -v -dir=/home/doctorme/Desktop/test_dir -scan=myList.lzt -s=myList.lzt -p=myList.lzt

		will do the following in this order:
			1. set the working directory to "/home/doctorme/Desktop/"
			2. print the help message
			3. print the version string
			4. scan all the files in "/home/doctorme/Desktop/" into a list and save the list to "/home/doctorme/Desktop/myList.lzt"
			5. sort "/home/doctorme/Desktop/myList.lzt" and save it
			6. print the contents of "/home/doctorme/Desktop/myList.lzt"
		note that the working directory is always set first before any other command executes.

	CLI COMMANDS:
		-h
		
			list out all available options
		
		-v
		
			Prints out the current version information.

		-s	: <existing_list_file.lzt>
		
			Sorts the given list alphabetically by name. If the "-dir" option is not used then the list file must
			be a fully qualified pathname
				i.e.	./main.py -s=/home/myUsername/Desktop/myList.lzt
						./main.py -dir=/home/myUsername/Desktop -s=someList.lzt

		-p	: <existing_list_file.lzt>
			
			Prints out the contents of the given .lzt file. You might use this to, say, dump out the contents into a
			.txt file for later perusal, or for some other tool to parse through. As with "-s" you can use the "-dir"
			option to specify the working directory.
				i.e.	./main.py -p=/home/myUsername/Desktop/someList.lzt
						./main.py -dir=/home/myUsername/Desktop -p=someList.lzt
		
		-dir :	<existing_path>
		
			Specify the working directory. Other commands will all operate on files in the working directory.
			The working directory is always set before any other command executes. If you're running multiple commands
			at the same time you can use this to shorten their arguments.
			
			The given path must exist, and it must be a fully qualified path name. You may not (at this time) use
			wildcards or special characters. (you can't do this: "-dir=/home/myUsername/Desk*" or "-dir=~/Desktop")
				i.e.	./main.py -dir=/home/myUsername/Desktop -
						
		-batren	: <someString> | <someString_###>
		
			Renames all the files in the current working directory. An extremely useful command for those of us
			who are obsessive compulsive about our collections! Be careful when you use this because you won't get
			a chance to change your mind afterward!
			
			Every file will be renamed to the given string.
			
			The "#" signs specify the number of digits that you want appended to the file name. Three "#" signs
			will give three digits: 000, 001, 002, 003 and so on. Four "#" signs means four digits: 0000, 0001 etc.
			
			If no "#" signs are given to specify the number of digits it will default to four.
		
		-tag	: <string_containing_tags>
		
			Append the given tag string to the end of all file names in the current directory. Note that the file extension
			will still be placed at the end of the file (so a .jpg will remain a .jpg; an .avi will still be an .avi)
			Tagging files like this is handy for sorting them out later.
				
		-scan	: <file_name.lzt>
		
			This handy little time saver will scan all the files in the current directory and build a list with the basic
			file info (name, size, extension, etc.) then save that list in the current directory (or at the given location
			if file_name is a fully qualified path).
			
			The resulting .lzt file's entries will be sorted alphabetically. Note that this sort takes a little time for
			very large .lzt files. If you don't care about the .lzt being sorted (or you're going to sort it later) then
			use the -fastscan command.
			
			Note that "scan" doesn't really fill in everything about a file, the data that it can gather is pretty sparse
			right now; but it still saves time compared to entering in scores of records by hand. The less obsessive
			compulsive of us will probably find "scan" to be very useful. As for myself, I tend to enter everything by hand
			because I love having all those details listed about what I have.
		
		-fastscan	: <file_name.lzt>
			
			Same thing as -scan except that the resulting .lzt file is not sorted. This saves a bit of time on big .lzt files.
				
		-killdups
		
			"-killdups" will destroy any duplicates of files in the current directory leaving you with only one copy
			of every file. It operates only on the current directory and does not go into subdirectories.
			
			Use this command on those monster picture directories that you fill up with thousands of nameless pictures
			and don't have enough time to sort through them all. This command may take some time to complete, especially
			if you run it on a directory with a lot of files in it.
			

PREFERENCES/.cfg:
	Catalogger supports some basic preferences that may make life easier or more comfortable for you.
	These preferences are stored in the "../hc.cfg" file right next to "../main.py". If you lose it or delete
	it don't worry: it'll be automatically regenerated with default settings.
	Here's a list of the preferences and what they do:
		sort_on_add : <True> | <False>
			Automatically sort the currently loaded list alphabetically by name when using the "add" command.
			
		sort_on_save : <True> | <False>
			Automatically sort alphabetically by name before saving.
			
		auto_mount_dir : <FULLY_QUALIFIED_PATHNAME>
			Automatically "cd" to this directory on startup. This must be a fully qualified pathname- open up
			your file browser, navigate to where you want your base to be and then copy paste from the address
			bar.
			
		auto_load : <True> | <False>
			Automatically load the last file you had open on startup.
			
		print_splash : <True> | <False>
			If true then the splash screen will print on startup; false and it won't.
			
		last_five :
			This is a list of the last five files you had open. If you open the .cfg file and edit it by hand
			be sure you leave this bit alone because it's handled automatically.

FAQ:
	Q: On startup I get some wierd message and then the program crashes and quits. What's up?
	A: This may be caused by an invalid setting in the .cfg file. Be sure that all the boolean stuff is either
		"True" or "False" and that all the strings are valid, fully qualified, pathnames. If you have any doubts
		then just delete the .cfg file and let the catalogger regenerate it.
		You might also want to be sure that you're using an up-to-date version of Python, that your terminal
		window is large enough to display the splash screen, and that you have all the right source files
		present in the "../bin" directory.
	
	Q: I try to use "-dir" to specify a directory but it keeps telling me that it's invalid. Those pathnames are hard so
		I type something like: "-dir=/home/my_user/Desktop/h*" and it says it doesn't exist. I checked it manually
		and I know for a fact that it's there!
	A: The "-dir" command in the catalogger is just a barely functional little thing that I cooked up. It's not
		nearly so nice as the "cd" command you may be familiar with from a fully fledged shell like bash or DOS.
		Try just typing out the full path, copy/pasting it from your file browser, or typing the fully qualified
		or relative paths to whatever your file is.
	
	Q: Scan doesn't bring in much data, is there some way to get it to fill in more fields?
	A: Actually, I'm already working on setting it up to bring in metadata from media files. Hopefully the next
		version of the catalogger will be able to tell not only that your copy of whatever has subtitles but
		also that those subs are in English and that the dubs are in German (or whatever). But this stuff is
		using an experimental Python module that doesn't fully work yet. The solution, if you must have it now,
		is to code it yourself. Take a look at the "scan_cwd()" function in "../bin/HC_sys.py".
	
	Q: I double-click very quickly sometimes on an entry in the directory navigation list (i.e. "..")
		but nothing happens. Is that a problem or what?
	A: Congratulations, you've found a bug. This appears to be a problem with the way tk handles mouse clicks. There's
		some sort of debounce issue where the double-click registers as an event but that event has nothing else
		associated with it (i.e. what was double clicked). The error message that used to print out has been silenced,
		but there's no cure other than to double-click with a little less zeal. Or you could use the "UP" button instead
		of the ".." entry in the list...
	
	Q: I double click on an entry in the directory navigation list but I can't navigate into it. Instead I stay
		where I am. Please fix this bug.
	A: That's not a bug. You're double clicking on a file, not a directory.
	
	Q: I ran the function to delete all duplicates in a directory that contains a lot of files (about 1000 files) and it
		doesn't seem to finish. Sometimes a few files pop up into the list, but the progress bar never gets to 100%.
	A: Give it some time. Perhaps a lot of time. If you're impatient then you might try splitting the directory into a
		bunch of smaller directories. Be sure you sort the big directory by file size before splitting it up otherwise
		you run the risk of putting copies of files into different sub-directories and thus defeating the purpose of the
		duplicate scanner. I've run this function on directories of about 400 files in about fifteen seconds, but big
		directories of 1000 files or more may take a long time. If your progress bar never reaches 100% then you may
		have found a bug. I could have sworn I checked this thing backwards and forwards, but it's possible.
	
	Q: When I run the Delete duplicates function the progress bar starts out slow and then speeds way up at the end.
		Is this normal or is it skipping files?
	A: This is completely normal. Files are scanned against the rest of the files in the directory. If no match is
		found then it is removed from the list of files to check so that subsequent files don't need to compare
		against it since it's already been checked. This means that the list of checkable files gets smaller and
		smaller and therefore quicker and quicker to check through.
		
	Q: When I open an older .lzt file that I made with a previous version of the cataloger (i.e. v3.5) and I try to edit
		an entry in the list the Edit dialog doesn't show the quality or the censorship status at all even though I
		know I set that.
	A: Older .lzt files don't use quite the same conventions as the newer ones. The "Cen" and "Quality" fields were being
		filled in with things like "uncen" and "excellent" when they should be "Uncensored" and "Excellent" respectively.
		You should be able to just re-mark those values or edit the .lzt file by hand to fix this.
