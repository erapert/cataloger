Cataloger
=========

<pre>
Program:	Cataloger v5.0
Author:		DoctorMe
e-mail:		erapert@gmail.com
Date:		01/27/2011
</pre>

What
====
This program provides utilities for cataloging and managing media files. It is meant to work on a traditional desktop (i.e. Linux or Windows). This program provides utilities such as:

* Find and delete duplicate files.
* Batch rename files in a directory following a number sequence.

Why
===
I'm writing this program for:

* My own satisfaction
* To improve and excersise my knowledge of Python
* To produce a useful tool for myself.
I share this program with you in the hope that it may also be useful to you in some way (through its intended function, or as an example to follow in your own coding adventures).

Warning
=======
I do use and test this program on my home machine using dummy files and real files. That is not to say that this program is foolproof or completely reliable in any respect!

So, obviously, if you've any doubts about the reliability of this program then:

* Don't use it on important files
* Don't use it period
* Or back up all your files until you're satisfied that this program is working correctly

Welcome
=======
Since I released v4.5 of this program on the internet and started really keeping track it has been downloaded more
than nine hundred times. I am proud. Thank you all for your interest. Thank you all for giving me this feeling of
satisfaction in my humble work.

License
=======
Since this program is written in Python and I'm distributing the .py files for it then you, by downloading this program, have direct access to the source code. If you're a programmer then feel free to modify my work as you please. If you're a non-programmer then I encourage you to learn. In this modern age knowing how to write code is invaluable.

* Please credit me if you modify or redistribute this program.
* If you fix any bugs or improve the code in some way then consider sending me your improvements through [github](https://github.com/doctorme/cataloger).
* However, I realize the nature of mankind and the internet: I don't really expect anyone to actually abide by the wishes of an anonymous programmer. The point is that I want you, whoever you are, to think about the effort I put into this program in the hope that someone may find it useful. Think about the nature of the gift I'm giving you.

Features/Bugfixes
=================

####changes/bugfixes scheduled for v6.0
- move to Python 3.2 and tkinter:ttk
- implement a dark color theme
- change code style to camel case rather than underscores
- add thumbnail view in "Delete duplicates" dialog to check duplicate files visually when selected in the listbox
- add second column to listbox in "Delete duplicates" dialog to list which files are duplicates of which
- add a button to scan a specific file into the currently loaded list (as opposed to scanning an entire directory at once)
- implement metadata scanning in scan function (i.e. to check language for subs/dubs)

####changes/bugfixes scheduled for v5.5
- add a "Tagging" dialog for images to the GUI: brings up a slideshow-like window. press the L and R arrows to progress, press 'c' to toggle "CEN", 's' to toggle dimensions, 'd' to toggle "DELETE", 'f' to toggle "FIND", 'e' toggles "ENG_SUBS", 'x' to toggle user defined tag
- add a "Cull" dialog to batch delete files which match certain criteria (i.e. resolution too small, file has [CEN] tag, is in certain format, etc)
- add a -cull command to CLI
- add an "Organize" dialog which uses tags to auto-sort a directory of files into sub directories based on their tags
- add an -organize command to CLI
- add e-mail account to ../README.md (coming out of the closet a little with this release. Don't make me regret it)
- completely re-write "Scan" function to work as a way to search/view media files based on queries (i.e. `show all files that are marked [CEN]` or `show all .png files` etc.). This means the end of the cataloging functionality.
+ README rewritten in markdown (displays nicer on github).
	
####changes/bugfixes in v5.0
+ remove CLI - replace with command line args. if (has_args) then execute them without starting GUI. Enables clean use from shell scripts etc.
+ add a -tag command to tag everything in a directory
+ update ../doc/INSTRUCTIONS.txt to remove the legacy CLI section and replace it with the new command line arguments and their usage
+ fix checkbox/radio buttons in the Add dialog-- the default values seem to be broken
+ renamer function needs to count the number of files in the current directory so that it allocates sufficient digit padding (2000 files can't be done with only three digits...)
+ renamer dialog no longer supports tags (tagging functionality is being re-done anyway)
+ sort files and directories alphabetically and seperately in the directory view panel
- _directory view panel: add a column to indicate `DIR` vs. `FILE` (needs multi-column listbox... use the DirList widget from Tix?? put it off 'till after the migration to ttk??)_
+ fix progress bar in "Scan Directory" dialog (it wasn't actually being used-- derp derp, that's what happens when you code instead of sleeping)
+ progress bars are now dark blue with white text instead eye-grating blue-and-yellow
+ fix auto-mount-on-startup directory in preferences
+ eliminate "tuple index out of range" bug (gui.py line 158)
+ startup is now slightly more robust (you can also run it through the Python IDE eric4)
- _after editing a selected record entry the detail box won't refresh to reflect the new value without re-selecting the entry in the list can't really remedy this because the listbox/detailbox lose focus when the Add/Edit dialog comes up and the listbox selection is wiped when the user selects something to edit in the dialog anyway_
+ add a removal/redeem button in "Delete duplicates" dialog to remove a duplicate from the list of files to be deleted (so that the redeemed file isn't deleted)
+ add a remove/delete button in the "Scan" dialog to remove items before they're added to the record list (would not delete actual files)
+ remove the "Edit" menu-- combine with the "List" menu
+ fixed file duplicate killer. runs slower but is more accurate and doesn't crash on directories containing very few files

####bugfixes 4.6
+ bug-fix and verify CLI- it won't start up
+ removed certain "branding" elements to make things more politically correct (I sent a copy to a prospective employer along with several other programs I'd written, got an interview, they called me back, and now I'm a professional programmer)
	
####changes in v4.5 ------- released 03/21/10 : 919 downloads as of 01/27/11!
+ prefs dialog
+ scan dir dialog
+ concat dialog
+ append dialog
+ kill dups dialog
+ renamer dialog
+ detail box for selected item in record list
+ help/about dialog
+ fixed progress bar in "Delete duplicates" dialog
+ fixed delete duplicates function (it now finds duplicates correctly)
+ added text feedback to "Delete duplicates" dialog to confirm that the process has finished

####changes in v4.0
+ implement base GUI (using Tkinter/Tk)
+ program menu bar (file, edit, list, tools)
+ directory navigation pane
+ list view pane
+ add, edit, delete buttons
+ up, go buttons

####changes in v3.5:
+ moved to an object oriented organization
+ added directory browsing within the cataloger (cd, dir, scan)
+ added 'scan' to load current working directory's contents into the currently loaded record list (major feature!)
+ added sorting by size
+ added mass file renaming (i.e. rename with sequential numbers, tentative support for tags)
+ improved the command line interface- it's easier to use now
+ improved OS support (there's only one version of the program now which runs on all operating systems)
+ faster sorting (using python's built-in sort method, which is a qsort algorithm)
+ changed the way file size is handled (it is kept as an integer number of bytes now rather than a string)
	
####v3.0 and earlier --- about 80 downloads
+ CLI only
+ released to 4chan.org/g/

